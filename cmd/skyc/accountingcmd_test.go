package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/types"
)

// randomAcountingInfo is a helper that generates random accounting information
func randomAcountingInfo(num int) []skymodules.AccountingInfo {
	var ais []skymodules.AccountingInfo
	for i := 0; i < num; i++ {
		ais = append(ais, skymodules.AccountingInfo{
			Wallet: skymodules.WalletAccounting{
				ConfirmedSiacoinBalance: types.NewCurrency64(fastrand.Uint64n(1000)),
				ConfirmedSiafundBalance: types.NewCurrency64(fastrand.Uint64n(1000)),
			},
			Renter: skymodules.RenterAccounting{
				UnspentUnallocated: types.NewCurrency64(fastrand.Uint64n(1000)),
				WithheldFunds:      types.NewCurrency64(fastrand.Uint64n(1000)),
			},
			Timestamp: time.Now().Unix(),
		})
	}
	return ais
}

// TestWriteAccountingCSV tests the writeAccountingCSV function
func TestWriteAccountingCSV(t *testing.T) {
	t.Parallel()

	// Create buffer
	var buf bytes.Buffer

	// Create accounting information
	ais := randomAcountingInfo(5)

	// Write the information to the csv file.
	err := writeAccountingCSV(ais, &buf)
	if err != nil {
		t.Fatal(err)
	}

	// Read the written data from the file.
	bytes := buf.Bytes()

	// Generated expected
	headerStr := strings.Join(accountingHeaders, ",")
	expected := fmt.Sprintf("%v\n", headerStr)
	for _, ai := range ais {
		timeStr := strconv.FormatInt(ai.Timestamp, 10)
		scStr := ai.Wallet.ConfirmedSiacoinBalance.String()
		sfStr := ai.Wallet.ConfirmedSiafundBalance.String()
		usStr := ai.Renter.UnspentUnallocated.String()
		whStr := ai.Renter.WithheldFunds.String()
		data := []string{timeStr, scStr, sfStr, usStr, whStr}
		dataStr := strings.Join(data, ",")
		expected = fmt.Sprintf("%v%v\n", expected, dataStr)
	}

	// Check bytes vs expected
	if expected != string(bytes) {
		t.Log("actual", string(bytes))
		t.Log("expected", expected)
		t.Fatal("unexpected")
	}
}

// TestAverageSCPriceAndTxnValue verifies the functionality of
// averageSCPriceAndTxnValue
func TestAverageSCPriceAndTxnValue(t *testing.T) {
	now := time.Now()
	before := now.AddDate(-1, 0, 0)
	after := now.AddDate(1, 0, 0)
	var tests = []struct {
		name               string
		scPrices           []SCPrice
		funds              types.Currency
		timestamp          time.Time
		expectedSCPAverage string
		expectAvgPrice     float64
	}{
		{"nil scpPrices", nil, types.SiacoinPrecision, now, "", 0},
		{"zero funds", nil, types.ZeroCurrency, now, "0", 0},
		{"after time window", []SCPrice{
			{Average: 1.00,
				EndTime:   before,
				StartTime: before,
			},
		}, types.SiacoinPrecision, now, "", 0},
		{"before time window", []SCPrice{
			{Average: 1.00,
				EndTime:   after,
				StartTime: after,
			},
		}, types.SiacoinPrecision, now, "", 0},
		{"happy case", []SCPrice{
			{Average: 1.00,
				EndTime:   after,
				StartTime: before,
			},
		}, types.SiacoinPrecision, now, "1", 1},
		{"2 decimal places", []SCPrice{
			{Average: 1.01,
				EndTime:   after,
				StartTime: before,
			},
		}, types.SiacoinPrecision, now, "1.01", 1.01},
		{"less than 1", []SCPrice{
			{Average: 0.01,
				EndTime:   after,
				StartTime: before,
			},
		}, types.SiacoinPrecision, now, "0.01", 0.01},
		{"less than 0.01", []SCPrice{
			{Average: 0.0001,
				EndTime:   after,
				StartTime: before,
			},
		}, types.SiacoinPrecision, now, "0.0001", 0.0001},
	}
	for _, test := range tests {
		scpAverage, avgPrice := averageSCPriceAndTxnValue(test.scPrices, test.funds, test.timestamp)
		if scpAverage != test.expectedSCPAverage {
			t.Fatal(test.name, "wrong scpAverage", scpAverage, test.expectedSCPAverage)
		}
		if avgPrice != test.expectAvgPrice {
			t.Fatal(test.name, "wrong avgPrice", avgPrice, test.expectAvgPrice)
		}
	}
}

// TestBlockAndTime tests the components of the blockAndTime function
func TestBlockAndTime(t *testing.T) {
	t.Run("Estimate", testBlockAndTimeEstiamte)
	t.Run("Exact", testBlockAndTimeExact)
}

// testBlockAndTimeEstiamte verifies the functionality of blockAndTimeEstimate
func testBlockAndTimeEstiamte(t *testing.T) {
	// Define today as end of February
	today := time.Date(2022, time.March, 0, 0, 0, 0, 0, time.UTC)

	// twoMonths is the rough number of seconds in 2 months. Using this as
	// the refHeight since the blockFrequency for testing is 1 sec.
	twoMonths := types.BlockHeight(1440 * 60 * 60)

	// Test start of month
	startBlock, startTime := blockAndTimeEstimate(today, time.January, twoMonths, true)

	// Get the expected Data
	date := time.Date(today.Year(), time.January, 1, 0, 0, 0, 0, today.Location())

	// Check Block
	secondsSinceDate := uint64(today.Sub(date).Seconds())
	blocksSinceStart := types.BlockHeight(secondsSinceDate) / types.BlockFrequency
	blockHeight := twoMonths - blocksSinceStart
	if blockHeight != startBlock {
		t.Fatalf("Unexpected block; expected %v, got %v", blockHeight, startBlock)
	}

	// Check Time
	expectedTime := date.Unix()
	if expectedTime != startTime {
		t.Fatalf("Unexpected time; expected %v, got %v", expectedTime, startTime)
	}

	// Test end of month
	endBlock, endTime := blockAndTimeEstimate(today, time.January, twoMonths, false)

	// Get the expected Data
	day := skymodules.DaysInMonth(time.January, today.Year())
	date = time.Date(today.Year(), time.January, day, 23, 59, 59, 0, today.Location())

	// Check Block
	secondsSinceDate = uint64(today.Sub(date).Seconds())
	blocksSinceStart = types.BlockHeight(secondsSinceDate) / types.BlockFrequency
	blockHeight = twoMonths - blocksSinceStart
	if blockHeight != endBlock {
		t.Fatalf("Unexpected block; expected %v, got %v", blockHeight, endBlock)
	}

	// Check Time
	expectedTime = date.Unix()
	if expectedTime != endTime {
		t.Fatalf("Unexpected time; expected %v, got %v", expectedTime, endTime)
	}
}

// testBlockAndTimeExact verifies the functionality of blockAndTimeExact
func testBlockAndTimeExact(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create a test node/client for this test group
	groupDir := skycTestDir(t.Name())
	n, err := newTestNode(groupDir)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := n.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	// Get a block and timeStamp
	equalHeight, err := n.BlockHeight()
	if err != nil {
		t.Fatal(err)
	}
	cbg, err := n.ConsensusBlocksHeightGet(equalHeight)
	if err != nil {
		t.Fatal(err)
	}
	equalTime := cbg.Timestamp

	// Sleep to ensure significant different between last block time stamp for testing and NDFs
	time.Sleep(2 * time.Second)

	// Grab a timestamp
	beforeTime := time.Now().Unix()

	// Sleep to ensure large gap between blocks for timestamp testing
	time.Sleep(2 * time.Second)

	// Mine some blocks so there are blocks after the timestamp
	for i := 0; i < 5; i++ {
		err = n.MineBlock()
		if err != nil {
			t.Fatal(err)
		}
		// Sleep in between blocks to ensure different timestamps
		time.Sleep(time.Second)
	}

	var tests = []struct {
		startingBlockHeight types.BlockHeight
		expectedBlockHeight types.BlockHeight
		dateTime            int64
		start               bool
	}{
		// Test Cases

		// Looking for start block, current block timestamp is before dateTime
		{equalHeight, equalHeight + 1, beforeTime, true},
		// Looking for start block, current block timestamp is after dateTime
		{equalHeight + 1, equalHeight + 1, beforeTime, true},
		// Looking for start block, current block timestamp is equal to dateTime
		{equalHeight, equalHeight, int64(equalTime), true},
		// Looking for end block, current block timestamp is before dateTime
		{equalHeight, equalHeight, beforeTime, false},
		// Looking for end block, current block timestamp is after dateTime
		{equalHeight + 1, equalHeight, beforeTime, false},
		// Looking for end block, current block timestamp is equal to dateTime
		{equalHeight, equalHeight, int64(equalTime), false},
	}
	for i, test := range tests {
		blockHeight, dateTimeReturn := blockAndTimeExact(n.Client, test.startingBlockHeight, test.dateTime, test.start)
		if blockHeight != test.expectedBlockHeight {
			t.Errorf("%v: Expected BH: %v, Actual BH: %v", i, test.expectedBlockHeight, blockHeight)
		}
		if dateTimeReturn != test.dateTime {
			t.Fatal("dateTime shouldn't change", dateTimeReturn, test.dateTime)
		}
	}
}
