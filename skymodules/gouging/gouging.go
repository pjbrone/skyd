package gouging

import (
	"fmt"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/types"
)

const (
	// downloadGougingFractionDenom sets the fraction to 1/4 because the renter
	// should have enough money to download at least a fraction of the amount of
	// data they intend to download. In practice, this ends up being a farily
	// weak gouging filter because a massive portion of the allowance tends to
	// be assigned to storage, and this does not account for that.
	downloadGougingFractionDenom = 4

	// pcwsGougingFractionDenom is used to identify what percentage of the
	// allowance is allowed to be spent on HasSector jobs before a worker is
	// flagged for being too expensive.
	//
	// For example, if the denom is 10, that means that if a worker's HasSector
	// cost multiplied by the total expected number of HasSector jobs to be
	// performed in a period exceeds 10% of the allowance, that worker will be
	// flagged for price gouging. If the denom is 100, the worker will be
	// flagged if the HasSector cost reaches 1% of the total cost of the
	// allowance.
	pcwsGougingFractionDenom = 25

	// sectorLookupToDownloadRatio is an arbitrary ratio that resembles the
	// amount of lookups vs downloads. It is used in price gouging checks.
	sectorLookupToDownloadRatio = 16

	// snapshotDownloadGougingFractionDenom sets the fraction to 1/100 because
	// downloading snapshots is important, so there is less sensitivity to
	// gouging. Also, this is a rare operation.
	snapshotDownloadGougingFractionDenom = 100

	// uploadGougingFractionDenom sets the gouging fraction to 1/4 based on the
	// idea that the user should be able to hit at least some fraction of their
	// desired upload volume using some fraction of hosts.
	uploadGougingFractionDenom = 4
)

var (
	// errHighSubscriptionMemoryCost is returned if the subscription gouging
	// check fails due to a high memory cost.
	errHighSubscriptionMemoryCost = errors.New("high SubscriptionMemoryCost")

	// errHighSubscriptionNotificationCost is returned if the subscription
	// gouging check fails due to a high notification cost.
	errHighSubscriptionNotificationCost = errors.New("high SubscriptionNotificationCost")
)

// CheckDownload looks at the current renter allowance and the active settings
// for a host and determines whether a backup fetch should be halted due to
// price gouging.
//
// NOTE: Currently this function treats all downloads being the stream download
// size and assumes that data is actually being appended to the host. As the
// worker gains more modification actions on the host, this check can be split
// into different checks that vary based on the operation being performed.
func CheckDownload(allowance skymodules.Allowance, pt modules.RPCPriceTable) error {
	// TODO (FILEBASE): disabled download gouging
	return nil

	// Check whether the base RPC price is too high.
	rpcCost := modules.MDMReadCost(&pt, skymodules.StreamDownloadSize)
	if !allowance.MaxRPCPrice.IsZero() && allowance.MaxRPCPrice.Cmp(rpcCost) < 0 {
		errStr := fmt.Sprintf("rpc price of host is %v, which is above the maximum allowed by the allowance: %v", rpcCost, allowance.MaxRPCPrice)
		return errors.New(errStr)
	}

	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(pt.DownloadBandwidthCost) < 0 {
		errStr := fmt.Sprintf("download bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v", pt.DownloadBandwidthCost, allowance.MaxDownloadBandwidthPrice)
		return errors.New(errStr)
	}

	// Check ReadLengthCost. This is unused by hosts right now and should be set to 1H.
	if types.NewCurrency64(1).Cmp(pt.ReadLengthCost) < 0 {
		errStr := fmt.Sprintf("ReadLengthCost of host is %v but should be %v", pt.ReadLengthCost, types.NewCurrency64(1))
		return errors.New(errStr)
	}

	// If there is no allowance, general price gouging checks have to be
	// disabled, because there is no baseline for understanding what might count
	// as price gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// Make sure the expected download is at least 1 to avoid multiplying
	// with 0 later.
	expectedDownload := allowance.ExpectedDownload
	if expectedDownload == 0 {
		expectedDownload++
	}

	// Check that the combined prices make sense in the context of the overall
	// allowance. The general idea is to compute the total cost of performing
	// the same action repeatedly until a fraction of the desired total resource
	// consumption established by the allowance has been reached. The fraction
	// is determined on a case-by-case basis. If the host is too expensive to
	// even satisfy a faction of the user's total desired resource consumption,
	// the action will be blocked for price gouging.
	singleDownloadCost := rpcCost.Add(pt.DownloadBandwidthCost.Mul64(skymodules.StreamDownloadSize))
	fullCostPerByte := singleDownloadCost.Div64(skymodules.StreamDownloadSize)
	allowanceDownloadCost := fullCostPerByte.Mul64(allowance.ExpectedDownload)
	reducedCost := allowanceDownloadCost.Div64(downloadGougingFractionDenom)
	if reducedCost.Cmp(allowance.Funds) > 0 {
		errStr := fmt.Sprintf("combined download pricing of host yields %v, which is more than the renter is willing to pay for the download: %v - price gouging protection enabled", reducedCost, allowance.Funds)
		return errors.New(errStr)
	}

	return nil
}

// CheckPCWS verifies the cost of grabbing the HasSector information from a host
// is reasonble. The cost of completing the download is not checked.
//
// NOTE: The logic in this function assumes that every pcws results in just one
// download. The reality is that depending on the type of use case, there may be
// significantly less than 1 download per pcws (for single-user nodes that
// frequently open large movies without watching the full movie), or
// significantly more than one download per pcws (for multi-user nodes where
// users most commonly are using the same file over and over).
func CheckPCWS(allowance skymodules.Allowance, pt modules.RPCPriceTable, numWorkers int, numRoots int) error {
	// TODO (FILEBASE): disabled download gouging
	return nil

	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(pt.DownloadBandwidthCost) < 0 {
		return fmt.Errorf("download bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v - price gouging protection enabled", pt.DownloadBandwidthCost, allowance.MaxDownloadBandwidthPrice)
	}
	// Check whether the upload bandwidth price is too high.
	if !allowance.MaxUploadBandwidthPrice.IsZero() && allowance.MaxUploadBandwidthPrice.Cmp(pt.UploadBandwidthCost) < 0 {
		return fmt.Errorf("upload bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v - price gouging protection enabled", pt.UploadBandwidthCost, allowance.MaxUploadBandwidthPrice)
	}
	// If there is no allowance, price gouging checks have to be disabled,
	// because there is no baseline for understanding what might count as price
	// gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// Calculate the cost of a has sector job.
	pb := modules.NewProgramBuilder(&pt, 0)
	for i := 0; i < numRoots; i++ {
		pb.AddHasSectorInstruction(crypto.Hash{})
	}
	programCost, _, _ := pb.Cost(true)
	ulbw, dlbw := HasSectorJobExpectedBandwidth(numRoots)
	bandwidthCost := modules.MDMBandwidthCost(pt, ulbw, dlbw)
	costHasSectorJob := programCost.Add(bandwidthCost)

	// Determine based on the allowance the number of HasSector jobs that would
	// need to be performed under normal conditions to reach the desired amount
	// of total data.
	requiredProjects := allowance.ExpectedDownload / skymodules.StreamDownloadSize
	requiredHasSectorQueries := requiredProjects * uint64(numWorkers)

	// Determine the total amount that we'd be willing to spend on all of those
	// queries before considering the host complicit in gouging.
	totalCost := costHasSectorJob.Mul64(requiredHasSectorQueries)
	reducedAllowance := allowance.Funds.Div64(pcwsGougingFractionDenom)

	// Check that we do not consider the host complicit in gouging.
	if totalCost.Cmp(reducedAllowance) > 0 {
		return errors.New("the cost of performing a HasSector job is too high - price gouging protection enabled")
	}
	return nil
}

// CheckProjectDownload verifies the cost of executing the jobs performed by the
// project download are reasonable in relation to the user's allowance and the
// amount of data they intend to download
func CheckProjectDownload(allowance skymodules.Allowance, pt modules.RPCPriceTable) error {
	// TODO (FILEBASE): disabled download gouging
	return nil

	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(pt.DownloadBandwidthCost) < 0 {
		return fmt.Errorf("download bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v - price gouging protection enabled", pt.DownloadBandwidthCost, allowance.MaxDownloadBandwidthPrice)
	}

	// Check whether the upload bandwidth price is too high.
	if !allowance.MaxUploadBandwidthPrice.IsZero() && allowance.MaxUploadBandwidthPrice.Cmp(pt.UploadBandwidthCost) < 0 {
		return fmt.Errorf("upload bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v - price gouging protection enabled", pt.UploadBandwidthCost, allowance.MaxUploadBandwidthPrice)
	}

	// If there is no allowance, price gouging checks have to be disabled,
	// because there is no baseline for understanding what might count as price
	// gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// In order to decide whether or not the cost of performing a PDBR is too
	// expensive, we make some assumptions with regards to lookup vs download
	// job ratio and avg download size. The total cost is then compared in
	// relation to the allowance, where we verify that a fraction of the cost
	// (which we'll call reduced cost) to download the amount of data the user
	// intends to download does not exceed its allowance.

	// Calculate the cost of a has sector job
	pb := modules.NewProgramBuilder(&pt, 0)
	pb.AddHasSectorInstruction(crypto.Hash{})
	programCost, _, _ := pb.Cost(true)

	ulbw, dlbw := HasSectorJobExpectedBandwidth(1)
	bandwidthCost := modules.MDMBandwidthCost(pt, ulbw, dlbw)
	costHasSectorJob := programCost.Add(bandwidthCost)

	// Calculate the cost of a read sector job, we use StreamDownloadSize as an
	// average download size here which is 64 KiB.
	pb = modules.NewProgramBuilder(&pt, 0)
	pb.AddReadSectorInstruction(skymodules.StreamDownloadSize, 0, crypto.Hash{}, true)
	programCost, _, _ = pb.Cost(true)

	ulbw, dlbw = ReadSectorJobExpectedBandwidth(skymodules.StreamDownloadSize)
	bandwidthCost = modules.MDMBandwidthCost(pt, ulbw, dlbw)
	costReadSectorJob := programCost.Add(bandwidthCost)

	// Calculate the cost of a project
	costProject := costReadSectorJob.Add(costHasSectorJob.Mul64(uint64(sectorLookupToDownloadRatio)))

	// Now that we have the cost of each job, and we estimate a sector lookup to
	// download ratio of 16, all we need to do is calculate the number of
	// projects necessary to download the expected download amount.
	numProjects := allowance.ExpectedDownload / skymodules.StreamDownloadSize

	// The cost of downloading is considered too expensive if the allowance is
	// insufficient to cover a fraction of the expense to download the amount of
	// data the user intends to download
	totalCost := costProject.Mul64(numProjects)
	reducedCost := totalCost.Div64(downloadGougingFractionDenom)
	if reducedCost.Cmp(allowance.Funds) > 0 {
		return fmt.Errorf("combined PDBR pricing of host yields %v, which is more than the renter is willing to pay for downloads: %v - price gouging protection enabled", reducedCost, allowance.Funds)
	}

	return nil
}

// CheckSnapshot looks at the current renter allowance and the active settings
// for a host and determines whether a snapshot upload should be halted due to
// price gouging.
func CheckSnapshot(allowance skymodules.Allowance, pt modules.RPCPriceTable) error {
	// TODO (FILEBASE): disabled gouging
	return nil

	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(pt.DownloadBandwidthCost) < 0 {
		errStr := fmt.Sprintf("download bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v", pt.DownloadBandwidthCost, allowance.MaxDownloadBandwidthPrice)
		return errors.New(errStr)
	}

	// If there is no allowance, general price gouging checks have to be
	// disabled, because there is no baseline for understanding what might count
	// as price gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// Check that the combined prices make sense in the context of the overall
	// allowance. The general idea is to compute the total cost of performing
	// the same action repeatedly until a fraction of the desired total resource
	// consumption established by the allowance has been reached. The fraction
	// is determined on a case-by-case basis. If the host is too expensive to
	// even satisfy a faction of the user's total desired resource consumption,
	// the action will be blocked for price gouging.
	expectedDL := modules.SectorSize
	rpcCost := modules.MDMInitCost(&pt, 48, 1).Add(modules.MDMReadCost(&pt, expectedDL)) // 48 bytes is the length of a single instruction read program
	bandwidthCost := pt.DownloadBandwidthCost.Mul64(expectedDL)
	fullCostPerByte := rpcCost.Add(bandwidthCost).Div64(expectedDL)
	allowanceDownloadCost := fullCostPerByte.Mul64(allowance.ExpectedDownload)
	reducedCost := allowanceDownloadCost.Div64(snapshotDownloadGougingFractionDenom)
	if reducedCost.Cmp(allowance.Funds) > 0 {
		errStr := fmt.Sprintf("combined download snapshot pricing of host yields %v, which is more than the renter is willing to pay for storage: %v - price gouging protection enabled", reducedCost, allowance.Funds)
		return errors.New(errStr)
	}

	return nil
}

// CheckSubscription checks that the host has reasonable prices set for
// subscribing to entries.
func CheckSubscription(allowance skymodules.Allowance, pt modules.RPCPriceTable) error {
	// TODO (FILEBASE): disabled download gouging
	return nil

	// Check the subscription related costs. These are hardcoded to 1 in the
	// host right now so we can just assume that they will always be 1. Once
	// they are configurable in the host, we can update this.
	if !pt.SubscriptionMemoryCost.Equals(types.NewCurrency64(1)) {
		return errors.AddContext(errHighSubscriptionMemoryCost, fmt.Sprintf("%v != 1", pt.SubscriptionMemoryCost))
	}
	if !pt.SubscriptionNotificationCost.Equals(types.NewCurrency64(1)) {
		return errors.AddContext(errHighSubscriptionNotificationCost, fmt.Sprintf("%v != 1", pt.SubscriptionMemoryCost))
	}
	// Use the download gouging check to make sure the bandwidth cost is
	// reasonable.
	return CheckProjectDownload(allowance, pt)
}

// CheckUpload looks at the current renter allowance and the active
// price table for a host and determines whether an upload should be halted due
// to price gouging.
func CheckUpload(allowance skymodules.Allowance, pt modules.RPCPriceTable) error {
	// TODO (FILEBASE): disabled gouging
	return nil

	// Check for download gouging first. If we can't download the chunk
	// there is no point in uploading it either.
	if err := CheckDownload(allowance, pt); err != nil {
		return errors.AddContext(err, "checkUploadGougingPT: failed download gouging check")
	}
	// Check MemoryTimeCost. This is unused by hosts right now and should be set to 1H.
	if types.NewCurrency64(1).Cmp(pt.MemoryTimeCost) < 0 {
		errStr := fmt.Sprintf("MemoryTimeCost of host is %v, which is above the maximum allowed by the allowance: %v", pt.MemoryTimeCost, types.NewCurrency64(1))
		return errors.New(errStr)
	}
	// Check WriteLengthCost. This is unused by hosts right now and should be set to 1H.
	if types.NewCurrency64(1).Cmp(pt.WriteLengthCost) < 0 {
		errStr := fmt.Sprintf("WriteLengthCost of host is %v, which is above the maximum allowed by the allowance: %v", pt.WriteLengthCost, types.NewCurrency64(1))
		return errors.New(errStr)
	}
	// Check InitBaseCost. This equals the BaseRPCPrice in the host settings.
	if !allowance.MaxRPCPrice.IsZero() && allowance.MaxRPCPrice.Cmp(pt.InitBaseCost) < 0 {
		errStr := fmt.Sprintf("InitBaseCost of host is %v, which is above the maximum allowed by the allowance: %v", pt.InitBaseCost, allowance.MaxRPCPrice)
		return errors.New(errStr)
	}
	// Check WriteBaseCost. This equals the SectorAccessPrice in the host settings.
	if !allowance.MaxSectorAccessPrice.IsZero() && allowance.MaxSectorAccessPrice.Cmp(pt.WriteBaseCost) < 0 {
		errStr := fmt.Sprintf("WriteBaseCost of host is %v, which is above the maximum allowed by the allowance: %v", pt.WriteBaseCost, allowance.MaxSectorAccessPrice)
		return errors.New(errStr)
	}
	// Check WriteStoreCost. This equals the StoragePrice in the host settings.
	if !allowance.MaxStoragePrice.IsZero() && allowance.MaxStoragePrice.Cmp(pt.WriteStoreCost) < 0 {
		errStr := fmt.Sprintf("WriteStoreCost of host is %v, which is above the maximum allowed by the allowance: %v", pt.WriteStoreCost, allowance.MaxStoragePrice)
		return errors.New(errStr)
	}
	// Check whether the upload bandwidth price is too high.
	if !allowance.MaxUploadBandwidthPrice.IsZero() && allowance.MaxUploadBandwidthPrice.Cmp(pt.UploadBandwidthCost) < 0 {
		errStr := fmt.Sprintf("UploadBandwidthPrice price of host is %v, which is above the maximum allowed by the allowance: %v", pt.UploadBandwidthCost, allowance.MaxUploadBandwidthPrice)
		return errors.New(errStr)
	}
	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(pt.DownloadBandwidthCost) < 0 {
		errStr := fmt.Sprintf("DownloadBandwidthPrice price of host is %v, which is above the maximum allowed by the allowance: %v", pt.DownloadBandwidthCost, allowance.MaxDownloadBandwidthPrice)
		return errors.New(errStr)
	}

	// If there is no allowance, general price gouging checks have to be
	// disabled, because there is no baseline for understanding what might count
	// as price gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// Cost of initializing the MDM.
	cost := modules.MDMInitCost(&pt, modules.SectorSize, 1)

	// Cost of executing a single sector append.
	memory := modules.MDMInitMemory() + modules.MDMAppendMemory()
	cost = cost.Add(modules.MDMMemoryCost(&pt, memory, modules.MDMTimeAppend))

	// Finalize the program.
	cost = cost.Add(modules.MDMMemoryCost(&pt, memory, modules.MDMTimeCommit))

	// Cost of storage and bandwidth.
	storageCost, _ := modules.MDMAppendCost(&pt, allowance.Period)
	bandwidthCost := modules.MDMBandwidthCost(pt, modules.SectorSize+2920, 2920) // assume sector data + 2 packets of overhead
	cost = cost.Add(storageCost).Add(bandwidthCost)

	// Cost of full upload.
	allowanceStorageCost := cost.Mul64(allowance.ExpectedStorage).Div64(modules.SectorSize)
	reducedCost := allowanceStorageCost.Div64(uploadGougingFractionDenom)
	if reducedCost.Cmp(allowance.Funds) > 0 {
		errStr := fmt.Sprintf("combined upload pricing of host yields %v, which is more than the renter is willing to pay for storage: %v - price gouging protection enabled", reducedCost, allowance.Funds)
		return errors.New(errStr)
	}
	return nil
}

// CheckUploadHES looks at the current renter allowance and the active settings
// for a host and determines whether an upload should be halted due to price
// gouging.
//
// NOTE: Currently this function treats all uploads as being the stream upload
// size and assumes that data is actually being appended to the host. As the
// worker gains more modification actions on the host, this check can be split
// into different checks that vary based on the operation being performed.
func CheckUploadHES(allowance skymodules.Allowance, pt *modules.RPCPriceTable, hostSettings modules.HostExternalSettings, allowSkipPT bool) error {
	// TODO (FILEBASE): disabled gouging
	return nil

	// Check for download gouging first. If we can't download the chunk
	// there is no point in uploading it either.
	if pt == nil && !allowSkipPT {
		return errors.New("pricetable missing")
	} else if pt != nil {
		if err := CheckDownload(allowance, *pt); err != nil {
			return errors.AddContext(err, "checkUploadGouging: failed download gouging check")
		}
	}
	// Check whether the base RPC price is too high.
	if !allowance.MaxRPCPrice.IsZero() && allowance.MaxRPCPrice.Cmp(hostSettings.BaseRPCPrice) < 0 {
		errStr := fmt.Sprintf("rpc price of host is %v, which is above the maximum allowed by the allowance: %v", hostSettings.BaseRPCPrice, allowance.MaxRPCPrice)
		return errors.New(errStr)
	}
	// Check whether the sector access price is too high.
	if !allowance.MaxSectorAccessPrice.IsZero() && allowance.MaxSectorAccessPrice.Cmp(hostSettings.SectorAccessPrice) < 0 {
		errStr := fmt.Sprintf("sector access price of host is %v, which is above the maximum allowed by the allowance: %v", hostSettings.SectorAccessPrice, allowance.MaxSectorAccessPrice)
		return errors.New(errStr)
	}
	// Check whether the storage price is too high.
	if !allowance.MaxStoragePrice.IsZero() && allowance.MaxStoragePrice.Cmp(hostSettings.StoragePrice) < 0 {
		errStr := fmt.Sprintf("storage price of host is %v, which is above the maximum allowed by the allowance: %v", hostSettings.StoragePrice, allowance.MaxStoragePrice)
		return errors.New(errStr)
	}
	// Check whether the upload bandwidth price is too high.
	if !allowance.MaxUploadBandwidthPrice.IsZero() && allowance.MaxUploadBandwidthPrice.Cmp(hostSettings.UploadBandwidthPrice) < 0 {
		errStr := fmt.Sprintf("upload bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v", hostSettings.UploadBandwidthPrice, allowance.MaxUploadBandwidthPrice)
		return errors.New(errStr)
	}
	// Check whether the download bandwidth price is too high.
	if !allowance.MaxDownloadBandwidthPrice.IsZero() && allowance.MaxDownloadBandwidthPrice.Cmp(hostSettings.DownloadBandwidthPrice) < 0 {
		errStr := fmt.Sprintf("download bandwidth price of host is %v, which is above the maximum allowed by the allowance: %v", hostSettings.UploadBandwidthPrice, allowance.MaxDownloadBandwidthPrice)
		return errors.New(errStr)
	}

	// If there is no allowance, general price gouging checks have to be
	// disabled, because there is no baseline for understanding what might count
	// as price gouging.
	if allowance.Funds.IsZero() {
		return nil
	}

	// Check that the combined prices make sense in the context of the overall
	// allowance. The general idea is to compute the total cost of performing
	// the same action repeatedly until a fraction of the desired total resource
	// consumption established by the allowance has been reached. The fraction
	// is determined on a case-by-case basis. If the host is too expensive to
	// even satisfy a fraction of the user's total desired resource consumption,
	// the action will be blocked for price gouging.
	singleUploadCost := hostSettings.SectorAccessPrice.Add(hostSettings.BaseRPCPrice).Add(hostSettings.UploadBandwidthPrice.Mul64(skymodules.StreamUploadSize)).Add(hostSettings.StoragePrice.Mul64(uint64(allowance.Period)).Mul64(skymodules.StreamUploadSize))
	fullCostPerByte := singleUploadCost.Div64(skymodules.StreamUploadSize)
	allowanceStorageCost := fullCostPerByte.Mul64(allowance.ExpectedStorage)
	reducedCost := allowanceStorageCost.Div64(uploadGougingFractionDenom)
	if reducedCost.Cmp(allowance.Funds) > 0 {
		errStr := fmt.Sprintf("combined upload pricing of host yields %v, which is more than the renter is willing to pay for storage: %v - price gouging protection enabled", reducedCost, allowance.Funds)
		return errors.New(errStr)
	}

	return nil
}
