package renter

import (
	"bytes"
	"sync"
)

const (
	// executeProgrammBufferSize defines the size of the buffers distributed
	// by the executeProgram buffer pool
	//
	// NOTE: we empirically found 4kib to be a safe value
	executeProgramBufferSize = 1 << 12 // 4kib

	// checkAccountBalanceBufferSize defines the size of the buffers distributed
	// by the checkAccountBalance buffer pool
	//
	// NOTE: we empirically found 400 bytes to be a safe value
	checkAccountBalanceBufferSize = 400

	// providePaymentBufferSize defines the size of the buffers distributed
	// by the providePayment buffer pool
	//
	// NOTE: we empirically found 300 bytes to be a safe value
	providePaymentBufferSize = 300
)

var (
	// lruBufferSize defines the size of the buffers distributed by the
	// staticLRUBufferPool. It's double the request size of the stream
	// buffer to account for the data plus potential overhead.
	lruBufferSize = int(2 * SkylinkDataSourceRequestSize)

	// staticPoolCheckAccountBalanceBuffers is the global pool for buffers used
	// by managedHostAccountBalance.
	staticPoolCheckAccountBalanceBuffers = newBufferPool(checkAccountBalanceBufferSize)

	// staticPoolExecuteProgramBuffers is the global pool for buffers used
	// by managedExecuteProgram.
	staticPoolExecuteProgramBuffers = newBufferPool(executeProgramBufferSize)

	// staticLRUBufferPool is a buffer pool for the persisted lru cache.
	staticLRUBufferPool = newBufferPool(lruBufferSize)

	// staticPoolProvidePaymentBuffers is the global pool for buffers used
	// by managedProvidePayment.
	staticPoolProvidePaymentBuffers = newBufferPool(providePaymentBufferSize)

	// staticPoolUnresolvedWorkers is the global pool for unresolvedWorker
	// objects.
	staticPoolUnresolvedWorkers = newUnresolvedWorkersPool()

	// staticPoolJobHasSectorResponse is the global pool for
	// jobHasSectorResponse objects.
	staticPoolJobHasSectorResponse = newJobHasSectorResponsePool()
)

type (
	// bufferPool defines a pool for buffers
	bufferPool struct {
		staticPool sync.Pool
	}

	// unresolvedWorkersPool defines a pool of unresolvedWorker objects.
	unresolvedWorkersPool struct {
		staticPool sync.Pool
	}

	// jobHasSectorResponsePool defines a pool of jobHasSectorResponse objects.
	jobHasSectorResponsePool struct {
		staticPool sync.Pool
	}
)

// newUnresolvedWorkersPool creates a new unresolvedWorkersPool.
func newUnresolvedWorkersPool() *unresolvedWorkersPool {
	return &unresolvedWorkersPool{
		staticPool: sync.Pool{
			New: func() interface{} {
				return &pcwsUnresolvedWorker{}
			},
		},
	}
}

// Get returns an unresolvedWorker from the pool.
func (p *unresolvedWorkersPool) Get() *pcwsUnresolvedWorker {
	return p.staticPool.Get().(*pcwsUnresolvedWorker)
}

// Put returns an unresolvedWorker to the pool after we are done with it.
func (p *unresolvedWorkersPool) Put(w *pcwsUnresolvedWorker) {
	p.staticPool.Put(w)
}

// newBufferPool creates a new pool with buffers of given size.
func newBufferPool(bufferSize int) *bufferPool {
	return &bufferPool{
		staticPool: sync.Pool{
			New: func() interface{} {
				return bytes.NewBuffer(make([]byte, bufferSize))
			},
		},
	}
}

// Get return a buffer from the pool and resets it beforehand.
func (p *bufferPool) Get() *bytes.Buffer {
	b := p.staticPool.Get().(*bytes.Buffer)
	b.Reset()
	return b
}

// Put returns a buffer to the pool.
func (p *bufferPool) Put(b *bytes.Buffer) {
	p.staticPool.Put(b)
}

// newJobHasSectorResponsePool create a new jobHasSectorResponsePool.
func newJobHasSectorResponsePool() *jobHasSectorResponsePool {
	return &jobHasSectorResponsePool{
		staticPool: sync.Pool{
			New: func() interface{} {
				return &jobHasSectorResponse{}
			},
		},
	}
}

// Get returns a jobHasSectorResponse from the pool.
func (p *jobHasSectorResponsePool) Get() *jobHasSectorResponse {
	return p.staticPool.Get().(*jobHasSectorResponse)
}

// Put returns a jobHasSectorResponse to the pool.
func (p *jobHasSectorResponsePool) Put(resp *jobHasSectorResponse) {
	p.staticPool.Put(resp)
}
