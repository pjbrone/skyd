package contractor

import (
	"math"
	"math/big"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"gitlab.com/SkynetLabs/skyd/skymodules/gouging"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/persist"
	"go.sia.tech/siad/types"
)

type utilityUpdateStatus int

const (
	_ = iota
	noUpdate
	suggestedUtilityUpdate
	necessaryUtilityUpdate
)

// Merge merges two update statuses into one. Whichever has the higher priority
// wins.
func (us utilityUpdateStatus) Merge(us2 utilityUpdateStatus) utilityUpdateStatus {
	if us > us2 {
		return us
	}
	return us2
}

// managedCheckHostScore checks host scorebreakdown against minimum accepted
// scores.  forceUpdate is true if the utility change must be taken.
func (c *Contractor) managedCheckHostScore(contract skymodules.RenterContract, sb skymodules.HostScoreBreakdown, minScoreGFR, minScoreGFU types.Currency) (skymodules.ContractUtility, utilityUpdateStatus) {
	c.mu.Lock()
	defer c.mu.Unlock()

	u := contract.Utility

	// Check whether the contract is a payment contract. Payment contracts
	// cannot be marked !GFR for poor score.
	var size uint64
	if len(contract.Transaction.FileContractRevisions) > 0 {
		size = contract.Transaction.FileContractRevisions[0].NewFileSize
	}
	paymentContract := !c.allowance.PaymentContractInitialFunding.IsZero() && size == 0

	// Contract has no utility if the score is poor. Cannot be marked as bad if
	// the contract is a payment contract.
	badScore := !minScoreGFR.IsZero() && sb.Score.Cmp(minScoreGFR) < 0
	if badScore && !paymentContract {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			c.staticLog.Printf("[CONTRACTUTILITY][%v] managedCheckHostScore failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
			c.staticLog.Println("Min Score:", minScoreGFR)
			c.staticLog.Println("Score:    ", sb.Score)
			c.staticLog.Println("Age Adjustment:        ", sb.AgeAdjustment)
			c.staticLog.Println("Base Price Adjustment: ", sb.BasePriceAdjustment)
			c.staticLog.Println("Burn Adjustment:       ", sb.BurnAdjustment)
			c.staticLog.Println("Collateral Adjustment: ", sb.CollateralAdjustment)
			c.staticLog.Println("Duration Adjustment:   ", sb.DurationAdjustment)
			c.staticLog.Println("Interaction Adjustment:", sb.InteractionAdjustment)
			c.staticLog.Println("Price Adjustment:      ", sb.PriceAdjustment)
			c.staticLog.Println("Storage Adjustment:    ", sb.StorageRemainingAdjustment)
			c.staticLog.Println("Uptime Adjustment:     ", sb.UptimeAdjustment)
			c.staticLog.Println("Version Adjustment:    ", sb.VersionAdjustment)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false

		c.staticLog.Println("Adding contract utility update to churnLimiter queue")
		return u, suggestedUtilityUpdate
	}

	// Contract should not be used for uploading if the score is poor.
	if !minScoreGFU.IsZero() && sb.Score.Cmp(minScoreGFU) < 0 {
		if u.GoodForUpload {
			c.staticLog.Printf("[CONTRACTUTILITY][%v] managedCheckHostScore failed, poor score, GFU: true -> false\n", contract.ID)
			c.staticLog.Println("Min Score:", minScoreGFU)
			c.staticLog.Println("Score:    ", sb.Score)
			c.staticLog.Println("Age Adjustment:        ", sb.AgeAdjustment)
			c.staticLog.Println("Base Price Adjustment: ", sb.BasePriceAdjustment)
			c.staticLog.Println("Burn Adjustment:       ", sb.BurnAdjustment)
			c.staticLog.Println("Collateral Adjustment: ", sb.CollateralAdjustment)
			c.staticLog.Println("Duration Adjustment:   ", sb.DurationAdjustment)
			c.staticLog.Println("Interaction Adjustment:", sb.InteractionAdjustment)
			c.staticLog.Println("Price Adjustment:      ", sb.PriceAdjustment)
			c.staticLog.Println("Storage Adjustment:    ", sb.StorageRemainingAdjustment)
			c.staticLog.Println("Uptime Adjustment:     ", sb.UptimeAdjustment)
			c.staticLog.Println("Version Adjustment:    ", sb.VersionAdjustment)
		}
		u.GoodForUpload = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// managedUtilityChecks performs checks on a contract that
// might require marking the contract as !GFR and/or !GFU.
// Returns the new utility and corresponding update status.
func (c *Contractor) managedUtilityChecks(contract skymodules.RenterContract, host skymodules.DecoratedHostDBEntry, sb skymodules.HostScoreBreakdown, minScoreGFU, minScoreGFR types.Currency, endHeight types.BlockHeight) (_ skymodules.ContractUtility, uus utilityUpdateStatus) {
	revision := contract.Transaction.FileContractRevisions[0]
	c.mu.RLock()
	allowance := c.allowance
	blockHeight := c.blockHeight
	renewWindow := c.allowance.RenewWindow
	period := c.allowance.Period
	_, renewed := c.renewedTo[contract.ID]
	c.mu.RUnlock()

	// Init uus to no update and the utility with the contract's utility.
	// We assume that the contract is good when we start.
	uus = noUpdate
	oldUtility := contract.Utility
	contract.Utility.GoodForRefresh = true
	contract.Utility.GoodForRenew = true
	contract.Utility.GoodForUpload = true

	// A contract with a dead score should not be used for anything.
	u, needsUpdate := deadScoreCheck(contract, sb.Score, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	// A contract that has been renewed should be set to !GFU and !GFR.
	u, needsUpdate = renewedCheck(contract, renewed, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = maxRevisionCheck(contract, revision.NewRevisionNumber, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = badContractCheck(contract, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = offlineCheck(contract, host.HostDBEntry, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = upForRenewalCheck(contract, renewWindow, blockHeight, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = sufficientFundsCheck(contract, host.HostDBEntry, period, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	if !c.staticDeps.Disrupt("DisableUDSGougingCheck") {
		u, needsUpdate = outOfStorageCheck(contract, blockHeight, c.staticLog)
		uus = uus.Merge(needsUpdate)
		contract.Utility = contract.Utility.Merge(u)
	}

	u, needsUpdate = udsGougingCheck(contract, allowance, host, revision.NewFileSize, endHeight, c.staticLog)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	u, needsUpdate = c.managedCheckHostScore(contract, sb, minScoreGFR, minScoreGFU)
	uus = uus.Merge(needsUpdate)
	contract.Utility = contract.Utility.Merge(u)

	if c.staticDeps.Disrupt("ContractForceNotGoodForUpload") {
		contract.Utility.GoodForUpload = false
		uus = necessaryUtilityUpdate
	}

	// Log some info about changed utilities.
	if oldUtility.BadContract != contract.Utility.BadContract {
		c.staticLog.Printf("[CONTRACTUTILITY][%v] badContract %v -> %v", contract.ID, oldUtility.BadContract, contract.Utility.BadContract)
	}
	if oldUtility.GoodForRefresh != contract.Utility.GoodForRefresh {
		c.staticLog.Printf("managedUtilityChecks: [%v] goodForRefresh %v -> %v", contract.ID, oldUtility.GoodForRefresh, contract.Utility.GoodForRefresh)
	}
	if oldUtility.GoodForRenew != contract.Utility.GoodForRenew {
		c.staticLog.Printf("[CONTRACTUTILITY][%v] goodForRenew %v -> %v", contract.ID, oldUtility.GoodForRenew, contract.Utility.GoodForRenew)
	}
	if oldUtility.GoodForUpload != contract.Utility.GoodForUpload {
		c.staticLog.Printf("[CONTRACTUTILITY][%v] goodForUpload %v -> %v", contract.ID, oldUtility.GoodForUpload, contract.Utility.GoodForUpload)
	}

	// If uus is no update but we still changed the utility that means the
	// state of the contract improved. We then set uus to "suggestedUpdate".
	if uus == noUpdate {
		uus = suggestedUtilityUpdate
	}
	return contract.Utility, uus
}

// managedHostInHostDBCheck checks if the host is in the hostdb and not
// filtered.  Returns true if a check fails and the utility returned must be
// used to update the contract state.
func (c *Contractor) managedHostInHostDBCheck(contract skymodules.RenterContract) (skymodules.DecoratedHostDBEntry, skymodules.ContractUtility, bool) {
	u := contract.Utility
	hostEntry, exists, err := c.staticHDB.Host(contract.HostPublicKey)
	host := skymodules.DecoratedHostDBEntry{HostDBEntry: hostEntry, PriceTable: c.staticHDB.PriceTable(contract.HostPublicKey)}

	// Contract has no utility if the host is not in the database. Or is
	// filtered by the blacklist or whitelist. Or if there was an error
	if !exists || host.Filtered || err != nil {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			c.staticLog.Printf("[CONTRACTUTILITY][%v] managedHostInHostDBCheck failed because found in hostDB: %v, or host is Filtered: %v, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, exists, host.Filtered, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false
		return host, u, true
	}

	// TODO: If the host is not in the hostdb, we need to do some sort of rescan
	// to recover the host. The hostdb is not supposed to be dropping hosts that
	// we have formed contracts with. We should do what we can to get the host
	// back.
	return host, u, false
}

// badContractCheck checks whether the contract has been marked as bad. If the
// contract has been marked as bad, GoodForUpload and GoodForRenew need to be
// set to false to prevent the renter from using this contract.
func badContractCheck(contract skymodules.RenterContract, logger *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	if u.BadContract {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			logger.Printf("[CONTRACTUTILITY][%v] badContractCheck failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// maxRevisionCheck will return a locked utility if the contract has reached its
// max revision.
func maxRevisionCheck(contract skymodules.RenterContract, revisionNumber uint64, logger *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	if revisionNumber == math.MaxUint64 {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			logger.Printf("[CONTRACTUTILITY][%v] maxRevisionCheck failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false
		u.Locked = true
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// offLineCheck checks if the host for this contract is offline.
// Returns true if a check fails and the utility returned must be used to update
// the contract state.
func offlineCheck(contract skymodules.RenterContract, host skymodules.HostDBEntry, log *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	// Contract has no utility if the host is offline.
	if isOffline(host) {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			log.Printf("[CONTRACTUTILITY][%v] offlineCheck failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// outOfStorageCheck checks if the host is running out of storage.
// Returns true if a check fails and the utility returned must be used to update
// the contract state.
func outOfStorageCheck(contract skymodules.RenterContract, blockHeight types.BlockHeight, log *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	// If LastOOSErr has never been set, return false.
	if u.LastOOSErr == 0 {
		return u, noUpdate
	}
	// Contract should not be used for uploading if the host is out of storage.
	if blockHeight-u.LastOOSErr <= oosRetryInterval {
		if u.GoodForUpload {
			log.Printf("[CONTRACTUTILITY][%v] outOfStorageCheck failed, GFU: true -> false\n", contract.ID)
		}
		u.GoodForUpload = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// deadScoreCheck will return a contract with no utility and a required update
// if the contract has a score <= 1.
func deadScoreCheck(contract skymodules.RenterContract, score types.Currency, logger *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	if score.Cmp(types.NewCurrency64(1)) <= 0 {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			logger.Printf("[CONTRACTUTILITY][%v] deadScoreCheck failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForRefresh = false
		u.GoodForUpload = false
		u.GoodForRenew = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// renewedCheck will return a contract with no utility and a required update if
// the contract has been renewed, no changes otherwise.
func renewedCheck(contract skymodules.RenterContract, renewed bool, logger *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	if renewed {
		// Log if the utility has changed.
		if u.GoodForUpload || u.GoodForRenew || u.GoodForRefresh {
			logger.Printf("[CONTRACTUTILITY][%v] renewedCheck failed, GFU: %v -> false, GFR: %v -> false, GFRef: %v -> false\n", contract.ID, u.GoodForUpload, u.GoodForRenew, u.GoodForRefresh)
		}
		u.GoodForUpload = false
		u.GoodForRefresh = false
		u.GoodForRenew = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// udsGougingCheck makes sure the host is not price gouging. UDS stands for
// upload, download and sector access.
// NOTE: The contractEndHeight argument is supposed to be the endHeight as
// perceived by the contractor when forming new contracts or renewing them. Not
// the actual endheight of the contract itself.
func udsGougingCheck(contract skymodules.RenterContract, allowance skymodules.Allowance, host skymodules.DecoratedHostDBEntry, contractSize uint64, contractEndHeight types.BlockHeight, logger *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	pt := host.PriceTable

	// use both gouging checks, download gouging is implicitly checked by both
	// upload gouging checks
	var err1 error
	if pt != nil {
		err1 = gouging.CheckUpload(allowance, *pt)
	}
	err2 := gouging.CheckUploadHES(allowance, pt, host.HostExternalSettings, true)
	if err := errors.Compose(err1, err2); err != nil {
		wasGFU := u.GoodForUpload
		wasGFR := u.GoodForRenew
		wasGFRef := u.GoodForRefresh

		markBadForRenew := contractSize > 0
		markBadForRefresh := markBadForRenew && contract.EndHeight < contractEndHeight

		// If the contract contains data don't renew to make sure we
		// don't pay for its storage.
		u.GoodForUpload = false
		if markBadForRenew {
			u.GoodForRenew = false
		}

		// If the contract contains data and a renew of the contract
		// would lead to its endHeight being extended, don't refresh it
		// anymore.
		if markBadForRefresh {
			u.GoodForRefresh = false
		}

		// Log if the utility has changed
		if wasGFU || (wasGFR && markBadForRenew) || (wasGFRef && markBadForRefresh) {
			logger.Printf("[CONTRACTUTILITY][%v] gouging check failed, err: %v, GFU: %v -> %v, GFR: %v -> %v, GFRef: %v -> %v\n", contract.ID, err, wasGFU, u.GoodForUpload, wasGFR, u.GoodForRenew, wasGFRef, u.GoodForRefresh)
		}
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// sufficientFundsCheck checks if there are enough funds left in the contract
// for uploads.
// Returns true if a check fails and the utility returned must be used to update
// the contract state.
func sufficientFundsCheck(contract skymodules.RenterContract, host skymodules.HostDBEntry, period types.BlockHeight, log *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility

	// Contract should not be used for uploading if the contract does
	// not have enough money remaining to perform the upload.
	blockBytes := types.NewCurrency64(modules.SectorSize * uint64(period))
	sectorStoragePrice := host.StoragePrice.Mul(blockBytes)
	sectorUploadBandwidthPrice := host.UploadBandwidthPrice.Mul64(modules.SectorSize)
	sectorDownloadBandwidthPrice := host.DownloadBandwidthPrice.Mul64(modules.SectorSize)
	sectorBandwidthPrice := sectorUploadBandwidthPrice.Add(sectorDownloadBandwidthPrice)
	sectorPrice := sectorStoragePrice.Add(sectorBandwidthPrice)
	percentRemaining, _ := big.NewRat(0, 1).SetFrac(contract.RenterFunds.Big(), contract.TotalCost.Big()).Float64()
	if contract.RenterFunds.Cmp(sectorPrice.Mul64(3)) < 0 || percentRemaining < MinContractFundUploadThreshold {
		if u.GoodForUpload {
			log.Printf("[CONTRACTUTILITY][%v] sufficientFundsCheck failed, insufficient funds, %v | %v, GFU: true -> false\n", contract.ID, contract.RenterFunds.Cmp(sectorPrice.Mul64(3)) < 0, percentRemaining)
		}
		u.GoodForUpload = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}

// upForRenewalCheck checks if this contract is up for renewal.
// Returns true if a check fails and the utility returned must be used to update
// the contract state.
func upForRenewalCheck(contract skymodules.RenterContract, renewWindow, blockHeight types.BlockHeight, log *persist.Logger) (skymodules.ContractUtility, utilityUpdateStatus) {
	u := contract.Utility
	// Contract should not be used for uploading if it's halfway through the
	// renew window. That way we don't lose all upload contracts as soon as
	// we hit the renew window and give them some time to be renewed while
	// still uploading. If uploading blocks renews for half a window,
	// uploading will be prevented and the contract will have the remaining
	// window to update.
	if blockHeight+renewWindow/2 >= contract.EndHeight {
		if u.GoodForUpload {
			log.Printf("[CONTRACTUTILITY][%v] upForRenewalCheck failed, it is time to renew the contract, GFU: true -> false\n", contract.ID)
		}
		u.GoodForUpload = false
		return u, necessaryUtilityUpdate
	}
	return u, noUpdate
}
