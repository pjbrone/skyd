package renter

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"
	"time"
	"unsafe"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/persist"
	"gitlab.com/SkynetLabs/skyd/siatest/dependencies"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/modules/host"
	"go.sia.tech/siad/types"
)

// assertAccountFn is a helper type that defines a function used to assert the
// delta between the account balance and its spending details after executing
// the given function
type assertAccountFn = func(fn func() error, category spendingCategory, delta types.Currency)

// newRandomHostKey creates a random key pair and uses it to create a
// SiaPublicKey, this method returns the SiaPublicKey alongisde the secret key
func newRandomHostKey() (types.SiaPublicKey, crypto.SecretKey) {
	sk, pk := crypto.GenerateKeyPair()
	return types.SiaPublicKey{
		Algorithm: types.SignatureEd25519,
		Key:       pk[:],
	}, sk
}

// TestAccount verifies the functionality of the account
func TestAccount(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	rt, err := newRenterTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		closedRenter := rt.renter
		if err := rt.Close(); err != nil {
			t.Error(err)
		}

		// these test are ran on a renter after Close has been called
		t.Run("Closed", func(t *testing.T) {
			testAccountClosed(t, closedRenter)
		})
		t.Run("CriticalOnDoubleSave", func(t *testing.T) {
			testAccountCriticalOnDoubleSave(t, closedRenter)
		})
	}()

	t.Run("AvailableBalance", testAccountAvailableBalance)
	t.Run("AvailableHostBalance", testAccountAvailableHostBalance)
	t.Run("CheckFundAccountGouging", testAccountCheckFundAccountGouging)
	t.Run("Constants", testAccountConstants)
	t.Run("ManagedCommitDeposit", testAccountManagedCommitDeposit)
	t.Run("ManagedCommitWithdrawal", testAccountManagedCommitWithdrawal)
	t.Run("MinMaxExpectedBalance", testAccountMinAndMaxExpectedBalance)
	t.Run("ResetSpending", testAccountResetSpending)
	t.Run("SyncBalance", testAccountSyncBalance)
	t.Run("TrackSpending", testAccountTrackSpending)

	t.Run("Creation", func(t *testing.T) { testAccountCreation(t, rt) })
	t.Run("Tracking", func(t *testing.T) { testAccountTracking(t, rt) })
}

// TestAccountAccounting verifies that the money spent and/or refunded from
// execution various jobs is properly reflected in the account balance and
// spending details
func TestAccountAccounting(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a disabled worker
	wt, err := newWorkerTesterCustomDependency(t.Name(), &dependencies.DependencyDisableWorker{}, skymodules.SkydProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := wt.Close(); err != nil {
			t.Fatal(err)
		}
	}()
	acc := wt.staticAccount

	// create a helper function that asserts a balance delta and spending delta
	assertDeltas := func(fn func() error, category spendingCategory, delta types.Currency) {
		acc.mu.Lock()
		balBefore := acc.balance
		speBefore := acc.spending.get(category)
		acc.mu.Unlock()

		err := fn()
		if err != nil && !strings.Contains(err.Error(), "could not find the desired sector") {
			t.Error(err)
			return
		}

		acc.mu.Lock()
		balAfter := acc.balance
		speAfter := acc.spending.get(category)
		acc.mu.Unlock()

		if balBefore.Sub(balAfter).Cmp(delta) != 0 {
			t.Errorf("unexpected balance delta %v-%v != %v (category=%v)", balBefore.HumanString(), balAfter.HumanString(), delta.HumanString(), category)
		}
		if speAfter.Sub(speBefore).Cmp(delta) != 0 {
			t.Errorf("unexpected spending delta %v-%v != %v (category=%v)", speAfter.HumanString(), speBefore.HumanString(), delta.HumanString(), category)
		}
	}

	// fetch a price table and refill the account
	wt.staticUpdatePriceTable()
	wt.managedRefillAccount()

	// get the pricetable
	pt := wt.staticPriceTable()

	// run sub-tests
	t.Run("HasSectorAccounting", func(t *testing.T) { testHasSectorAccounting(t, wt, pt, assertDeltas) })
	t.Run("ReadRegistryAccounting", func(t *testing.T) { testReadRegistryAccounting(t, wt, pt, assertDeltas) })
	t.Run("ReadSectorAccounting", func(t *testing.T) { testReadSectorAccounting(t, wt, pt, assertDeltas) })
	t.Run("UpdateRegistryAccounting", func(t *testing.T) { testUpdateRegistryAccounting(t, wt, pt, assertDeltas) })
}

// testHasSectorAccounting tests that a successfully has sector job withdraws
// the right amount from the balance and properly updates the account's spending
// details.
func testHasSectorAccounting(t *testing.T, wt *workerTester, pt *workerPriceTable, assertFn assertAccountFn) {
	// compute the expected cost
	pb := modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	pb.AddHasSectorInstruction(crypto.Hash{})
	cost, _, _ := pb.Cost(true)

	// add the expected bandwidth cost
	//
	// NOTE: We use 1460 here because we know that that's the actual bandwidth
	// we are using. HasSectorJobExpectedBandwidth will slightly overestimate
	// the bandwidth by using 1500 and then issue a refund for 40 bytes.
	bandwidthCost := modules.MDMBandwidthCost(pt.staticPriceTable, 1460, 1460)
	cost = cost.Add(bandwidthCost)

	assertFn(func() error {
		responseChan := make(chan *jobHasSectorResponse)
		j := wt.newJobHasSector(context.Background(), responseChan, 10, crypto.Hash{})
		if !wt.externLaunchAsyncJob(j) {
			return errors.New("job wasn't launched")
		}
		resp := <-responseChan
		if resp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryDownload, cost)
}

// testReadRegistryAccounting tests that a successfully read registry job
// withdraws the right amount from the balance and properly updates the
// account's spending details. Both a job that finds an entry and one that
// doesn't
func testReadRegistryAccounting(t *testing.T, wt *workerTester, pt *workerPriceTable, assertFn assertAccountFn) {
	// create a registry value.
	rv, spk, _ := randomRegistryValue()

	// update the registry
	updateChan := make(chan *jobUpdateRegistryResponse)
	j := wt.newJobUpdateRegistry(context.Background(), testSpan(), updateChan, spk, rv)
	if !wt.externLaunchAsyncJob(j) {
		t.Fatal("job wasn't launched")
	}
	resp := <-updateChan
	if resp.staticErr != nil {
		t.Fatal(resp.staticErr)
	}

	// compute the expected cost
	pb := modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	eid := modules.DeriveRegistryEntryID(spk, rv.Tweak)
	_, err := pb.AddReadRegistryEIDInstruction(eid, true, modules.ReadRegistryVersionWithType)
	if err != nil {
		t.Fatal(err)
	}
	cost, _, _ := pb.Cost(true)

	// add the expected bandwidth cost
	//
	// NOTE: We use 1460 here because we know that that's the actual
	// bandwidth we are using. readRegistryJobExpectedBandwidth will
	// slightly overestimate the bandwidth by using 1500 and then issue a
	// refund for 40 bytes.
	bandwidthCost := modules.MDMBandwidthCost(pt.staticPriceTable, 1460, 1460)
	cost = cost.Add(bandwidthCost)

	assertFn(func() error {
		readChan := make(chan *jobReadRegistryResponse)
		jr := wt.newJobReadRegistryEID(context.Background(), testSpan(), readChan, eid, nil, nil)
		if !wt.externLaunchAsyncJob(jr) {
			return errors.New("job wasn't launched")
		}
		readResp := <-readChan
		if readResp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryRegistryRead, cost)

	// compute the expected cost for a missing entry
	pb = modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	missingEntryRefund, err := pb.AddReadRegistryEIDInstruction(eid, true, modules.ReadRegistryVersionWithType)
	if err != nil {
		t.Fatal(err)
	}
	cost, _, _ = pb.Cost(true)

	// subtract the refund for the entry not being found.
	cost = cost.Sub(missingEntryRefund)

	// add the expected bandwidth cost. Same as before.
	cost = cost.Add(bandwidthCost)

	assertFn(func() error {
		readChan := make(chan *jobReadRegistryResponse)
		jr := wt.newJobReadRegistryEID(context.Background(), testSpan(), readChan, modules.RegistryEntryID{}, nil, nil)
		if !wt.externLaunchAsyncJob(jr) {
			return errors.New("job wasn't launched")
		}
		readResp := <-readChan
		if readResp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryRegistryRead, cost)
}

// testReadSectorAccounting tests that a successfully read sector job withdraws
// the right amount from the balance and properly updates the account's spending
// details.
func testReadSectorAccounting(t *testing.T, wt *workerTester, pt *workerPriceTable, assertFn assertAccountFn) {
	// add sector data to the host
	sectorData := fastrand.Bytes(int(modules.SectorSize))
	sectorRoot := crypto.MerkleRoot(sectorData)
	err := wt.host.AddSector(sectorRoot, sectorData)
	if err != nil {
		t.Fatal(err)
	}

	// compute the expected cost
	pb := modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	pb.AddReadSectorInstruction(modules.SectorSize, 0, sectorRoot, true)
	cost, _, _ := pb.Cost(true)

	// add the expected bandwidth cost
	bandwidthCost := modules.MDMBandwidthCost(pt.staticPriceTable, 1460, 4380)
	cost = cost.Add(bandwidthCost)

	assertFn(func() error {
		jobMetadata := jobReadMetadata{
			staticWorker:           wt.worker,
			staticSectorRoot:       sectorRoot,
			staticSpendingCategory: categoryDownload,
		}

		responseChan := make(chan *jobReadResponse)
		j := wt.newJobReadSector(context.Background(), wt.worker.staticJobReadQueue, responseChan, jobMetadata, sectorRoot, 0, modules.SectorSize)
		if !wt.externLaunchAsyncJob(j) {
			return errors.New("job wasn't launched")
		}
		resp := <-responseChan
		if resp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryDownload, cost)

	// compute the expected cost
	pb = modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	pb.AddReadSectorInstruction(modules.SectorSize, 0, sectorRoot, true)
	cost, _, _ = pb.Cost(true)

	// add the expected bandwidth cost
	bandwidthCost = modules.MDMBandwidthCost(pt.staticPriceTable, 1460, 1460)
	cost = cost.Add(bandwidthCost)

	// assert the cost of fetching a missing sector
	assertFn(func() error {
		jobMetadata := jobReadMetadata{
			staticWorker:           wt.worker,
			staticSectorRoot:       crypto.Hash{},
			staticSpendingCategory: categoryDownload,
		}

		responseChan := make(chan *jobReadResponse)
		j := wt.newJobReadSector(context.Background(), wt.worker.staticJobReadQueue, responseChan, jobMetadata, crypto.Hash{}, 0, modules.SectorSize)
		if !wt.externLaunchAsyncJob(j) {
			return errors.New("job wasn't launched")
		}
		resp := <-responseChan
		if resp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryDownload, cost)
}

// testUpdateRegistryAccounting tests that a successfully update registry job
// withdraws the right amount from the balance and properly updates the
// account's spending details.
func testUpdateRegistryAccounting(t *testing.T, wt *workerTester, pt *workerPriceTable, assertFn assertAccountFn) {
	// create a registry value.
	rv, spk, _ := randomRegistryValue()

	// compute the expected cost
	pb := modules.NewProgramBuilder(&pt.staticPriceTable, 0)
	err := pb.AddUpdateRegistryInstruction(spk, rv)
	if err != nil {
		t.Fatal(err)
	}
	cost, _, _ := pb.Cost(true)

	// add the expected bandwidth cost
	//
	// NOTE: We use 1460 here because we know that that's the actual
	// bandwidth we are using. updateRegistryJobExpectedBandwidth will
	// slightly overestimate the bandwidth by using 1500 and then issue a
	// refund for 40 bytes.
	bandwidthCost := modules.MDMBandwidthCost(pt.staticPriceTable, 1460, 1460)
	cost = cost.Add(bandwidthCost)

	assertFn(func() error {
		updateChan := make(chan *jobUpdateRegistryResponse)
		j := wt.newJobUpdateRegistry(context.Background(), testSpan(), updateChan, spk, rv)
		if !wt.externLaunchAsyncJob(j) {
			return errors.New("job wasn't launched")
		}
		resp := <-updateChan
		if resp.staticErr != nil {
			return resp.staticErr
		}
		return nil
	}, categoryRegistryWrite, cost)
}

// TestAccountAlerts verifies the worker account properly registers and
// unregisters alerts that indicate a problem with the renter's ephemeral
// account balance tracking
func TestAccountAlerts(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// compose two dependencies into one, the result is a dependency that disables the worker,
	// and allows toggling a zero host balance to be returned
	parent := &dependencies.DependencyDisableWorker{}
	depZeroBalance := dependencies.NewDependencyZeroBalance()
	deps := dependencies.NewMultiDependency(parent, depZeroBalance)

	// create the worker tester
	wt, err := newWorkerTesterCustomDependency(t.Name(), deps, modules.ProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := wt.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	// fetch the balance target
	balanceTarget := wt.worker.staticRenter.staticAccountBalanceTarget

	// create two small helper functions that indicate whether our alerts are registered or not
	isDriftAlertRegistered := func() (registered bool) {
		_, _, warnings, _ := wt.staticRenter.staticAlerter.Alerts()
		for _, alert := range warnings {
			if strings.Contains(alert.Msg, "host balance has drifted away from the renter balance") {
				registered = true
			}
		}
		return
	}
	isBalanceAlertRegistered := func() (registered bool) {
		_, errs, _, _ := wt.staticRenter.staticAlerter.Alerts()
		for _, alert := range errs {
			if strings.Contains(alert.Msg, "the ephemeral account is out of balance") {
				registered = true
			}
		}
		return
	}

	// assert both alert are not registered
	if isDriftAlertRegistered() || isBalanceAlertRegistered() {
		t.Fatal("unexpected alert registered", isDriftAlertRegistered(), isBalanceAlertRegistered())
	}

	// fetch a price table and refill the acount manually
	wt.staticUpdatePriceTable()
	wt.managedRefillAccount()

	// sync the account balance with the host
	wt.externSyncAccountBalanceToHost(false)

	// assert the alert is registered
	if !isDriftAlertRegistered() {
		t.Fatal("expected alert to be registered")
	}

	// disable our dependency
	depZeroBalance.Disable()

	// sync the account balance with the host
	wt.externSyncAccountBalanceToHost(false)

	// assert the alert gets unregistered
	if isDriftAlertRegistered() {
		t.Fatal("expected alert to be unregistered")
	}

	// assert balance is filled and not zero
	balanceBefore := wt.worker.staticAccount.managedAvailableBalance()
	if !balanceBefore.Equals(balanceTarget) || balanceTarget.IsZero() {
		t.Fatal("unexpected account balance", balanceBefore)
	}

	// mock money being spent
	withdrawal := balanceBefore.MulFloat(.1)
	wt.worker.staticAccount.managedTrackWithdrawal(withdrawal)
	wt.worker.staticAccount.managedCommitWithdrawal(categoryDownload, withdrawal, types.ZeroCurrency, nil)

	// sync the account balance with the host
	wt.externSyncAccountBalanceToHost(false)

	// assert the alert gets unregistered
	if isDriftAlertRegistered() {
		t.Fatal("expected alert to be unregistered")
	}

	// assert balance is resest
	balanceAfter := wt.worker.staticAccount.managedAvailableBalance()
	if !balanceAfter.Equals(balanceTarget) {
		t.Fatal("unexpected account balance", balanceAfter)
	}

	// re-enable our dependency
	depZeroBalance.Enable()

	// create a dummy program
	pt := wt.worker.staticPriceTable().staticPriceTable
	pb := modules.NewProgramBuilder(&pt, 0)
	pb.AddHasSectorInstruction(crypto.Hash{})
	p, data := pb.Program()
	cost, _, _ := pb.Cost(true)
	jhs := new(jobHasSector)
	jhs.staticSectors = []crypto.Hash{{1, 2, 3}}
	ulBandwidth, dlBandwidth := jhs.callExpectedBandwidth()
	bandwidthCost, bandwidthRefund := mdmBandwidthCost(pt, ulBandwidth, dlBandwidth)
	cost = cost.Add(bandwidthCost)

	// execute it and assert we got the error we expected
	_, _, err = wt.worker.managedExecuteProgram(p, data, types.FileContractID{}, categoryDownload, cost, bandwidthRefund)
	if !isInsufficientBalanceError(err) {
		t.Fatal("unexpected error", err)
	}

	// assert the alert is registered
	if !isBalanceAlertRegistered() {
		t.Fatal("expected alert to be registered")
	}

	// disable our dependency
	depZeroBalance.Disable()

	// execute the progream again
	_, _, err = wt.worker.managedExecuteProgram(p, data, types.FileContractID{}, categoryDownload, cost, bandwidthRefund)
	if err != nil {
		t.Fatal("unexpected error", err)
	}

	// assert the alert is unregistered
	if isBalanceAlertRegistered() {
		t.Fatal("expected alert to be unregistered")
	}
}

// TestSpendingDetails is a unit test that verifies the functionality of the
// spending details helper struct
func TestSpendingDetails(t *testing.T) {
	t.Parallel()

	// NOTE: we use reflection to both populate the spending details and
	// calculate the expected sum, this way we make sure that when the struct
	// fields are extended, the sum function is taken into account

	var s spendingDetails

	// initialize the struct with random currencies for all fields
	rv := reflect.ValueOf(&s).Elem()
	for i := 0; i < rv.NumField(); i++ {
		field := rv.FieldByName(rv.Type().Field(i).Name)
		randomCurrency := types.NewCurrency64(fastrand.Uint64n(1e3))
		reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).
			Elem().
			Set(reflect.ValueOf(randomCurrency))
	}

	// calculate the expected sum
	var expected types.Currency
	for i := 0; i < rv.NumField(); i++ {
		field := rv.FieldByName(rv.Type().Field(i).Name)
		value := reflect.NewAt(field.Type(), unsafe.Pointer(field.UnsafeAddr())).Elem().Interface()
		expected = expected.Add(value.(types.Currency))
	}

	// assert the expected sum is not zero
	if expected.IsZero() {
		t.Fatalf("unexpected zero expected sum")
	}

	// assert the sum matches the expected value
	if !expected.Equals(s.sum()) {
		t.Fatalf("unexpected sum, %v != %v", s.sum().HumanString(), expected.HumanString())
	}
}

// TestWorkerAccount verifies the functionality of the account related
// functionality of the worker.
func TestWorkerAccount(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	wt, err := newWorkerTester(t.Name())
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	t.Run("HostAccountBalance", func(t *testing.T) {
		testWorkerAccountHostAccountBalance(t, wt)
	})
	t.Run("SyncAccountBalanceToHostCritical", func(t *testing.T) {
		testWorkerAccountSyncAccountBalanceToHostCritical(t, wt)
	})
	t.Run("SpendingDetails", func(t *testing.T) {
		testWorkerAccountSpendingDetails(t, wt)
	})
}

// TestWorkerAccountForcedSync verifies whether a forced sync executes an
// account balance sync ahead of schedule.
func TestWorkerAccountForcedSync(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// prepare our dependencies, one that makes the host return a zero balance
	// and two others that disable refills or price table updates
	depsZB := dependencies.NewDependencyZeroBalance()
	depsDR := dependencies.NewDependencyDisableRefill()
	depsDUPT := dependencies.NewDependencyDisableUpdatePriceTable()

	// disable our dependencies
	depsZB.Disable()
	depsDR.Disable()
	depsDUPT.Disable()

	// create a worker tester with our dependencies
	wt, err := newWorkerTesterCustomDependency(t.Name(), dependencies.NewMultiDependency(depsZB, depsDR, depsDUPT), modules.ProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// convenience variables
	w := wt.worker
	a := w.staticAccount
	wms := w.staticMaintenanceState

	// assert the account has been synced
	if a.callNeedsToSync() {
		t.Fatal("account should have been synced")
	}

	// assert a forced sync has not been scheduled
	if a.callForcedSyncScheduled() {
		t.Fatal("account should not have a forced sync scheduled")
	}

	// assert the balance is not zero
	if a.managedAvailableBalance().IsZero() {
		t.Fatal("account balance should not be zero")
	}

	// enable our dependency
	depsZB.Enable()
	depsDR.Enable()

	// we mock a maintenance cooldown here to ensure the forced account sync
	// overrules it
	wms.mu.Lock()
	wms.accountSyncSucceeded = false
	wms.cooldownUntil = time.Now().Add(time.Minute)
	wms.mu.Unlock()
	if !w.managedOnMaintenanceCooldown() {
		t.Fatal("worker should be on a maintenance cooldown")
	}

	// schedule a forced sync
	w.callScheduleForcedAccountSync()

	// assert a forced sync has been scheduled
	if !a.callForcedSyncScheduled() {
		t.Fatal("account should have a forced sync scheduled")
	}

	// assert a forced sync was triggered
	err = build.Retry(100, 100*time.Millisecond, func() error {
		// assert the balance is now zero
		if !a.managedAvailableBalance().IsZero() {
			return errors.New("account balance should be zero")
		}

		// assert the forced sync field has been reset
		if a.callForcedSyncScheduled() {
			return errors.New("account forced sync field should have been reset")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// disable our dependency
	depsZB.Disable()

	// schedule a non-forced sync
	a.mu.Lock()
	a.syncAt = time.Now().Add(-time.Second)
	a.mu.Unlock()

	// assert the balance restored
	err = build.Retry(100, 100*time.Millisecond, func() error {
		// assert the balance is not zero
		if a.managedAvailableBalance().IsZero() {
			w.staticWake() // wake the worker
			return errors.New("account balance should not be zero")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// block price table updates and invalidate the current price table
	depsDUPT.Enable()
	pt := *w.staticPriceTable()
	pt.staticUpdateTime = time.Now().Add(-time.Second)
	pt.staticExpiryTime = time.Now().Add(-time.Second)
	w.staticSetPriceTable(&pt)

	// schedule a forced sync
	w.callScheduleForcedAccountSync()

	// assert the forced sync was scheduled
	if !a.callForcedSyncScheduled() {
		t.Fatal("unexpected")
	}

	// assert the forced sync is not being triggered, as it would fail because
	// we don't have a valid price table anyway
	err = build.Retry(10, 100*time.Millisecond, func() error {
		if !a.callForcedSyncScheduled() {
			return errors.New("should not have been reset")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// disable our dep again
	depsDUPT.Disable()

	// assert the forced sync was executed
	err = build.Retry(100, 100*time.Millisecond, func() error {
		if a.callForcedSyncScheduled() {
			return errors.New("should have been reset")
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// testAccountCheckFundAccountGouging checks that `checkFundAccountGouging` is
// correctly detecting price gouging from a host.
func testAccountCheckFundAccountGouging(t *testing.T) {
	t.Parallel()

	// allowance contains only the fields necessary to test the price gouging
	allowance := skymodules.Allowance{
		Funds: types.SiacoinPrecision.Mul64(1e3),
	}

	// set the target balance to 1SC, this is necessary because this decides how
	// frequently we refill the account, which is a required piece of knowledge
	// in order to estimate the total cost of refilling
	targetBalance := types.SiacoinPrecision

	// verify happy case
	pt := newDefaultPriceTable()
	err := checkFundAccountGouging(pt, allowance, targetBalance)
	if err != nil {
		t.Fatal("unexpected price gouging failure")
	}

	// verify gouging case, in order to do so we have to set the fund account
	// cost to an unreasonable amount, empirically we found 75mS to be such a
	// value for the given parameters (1000SC funds and TB of 1SC)
	pt = newDefaultPriceTable()
	pt.FundAccountCost = types.SiacoinPrecision.MulFloat(0.075)
	err = checkFundAccountGouging(pt, allowance, targetBalance)
	if err == nil || !strings.Contains(err.Error(), "fund account cost") {
		t.Fatalf("expected fund account cost gouging error, instead error was '%v'", err)
	}
}

// testAccountConstants makes sure that certain relationships between constants
// exist.
func testAccountConstants(t *testing.T) {
	// Sanity check that the metadata size is not larger than the account size.
	if metadataSize > accountSize {
		t.Fatal("metadata size is larger than account size")
	}
	if accountSize > 4096 {
		t.Fatal("account size must not be larger than a disk sector")
	}
	if 4096%accountSize != 0 {
		t.Fatal("account size must be a factor of 4096")
	}
}

// testAccountAvailableBalance is a small unit test that verifies the
// functionality of the available balance method
func testAccountAvailableBalance(t *testing.T) {
	t.Parallel()

	oneCurrency := types.NewCurrency64(1)

	a := new(account)
	a.balance = oneCurrency
	a.negativeBalance = oneCurrency
	if !a.availableBalance().IsZero() {
		t.Fatal("unexpected available balance")
	}
	a.pendingDeposits = oneCurrency
	if a.availableBalance().IsZero() {
		t.Fatal("unexpected available balance")
	}
	a.pendingWithdrawals = oneCurrency
	if !a.availableBalance().IsZero() {
		t.Fatal("unexpected available balance")
	}
}

// testAccountAvailableHostBalance is a small unit test that verifies the
// functionality of the available host balance method
func testAccountAvailableHostBalance(t *testing.T) {
	t.Parallel()

	oneCurrency := types.NewCurrency64(1)

	a := new(account)
	a.hostBalance = oneCurrency
	a.hostBalanceNegative = oneCurrency
	if !a.availableHostBalance().IsZero() {
		t.Fatal("unexpected available host balance")
	}
	a.pendingDeposits = oneCurrency
	if a.availableBalance().IsZero() {
		t.Fatal("unexpected available host balance")
	}
	a.pendingWithdrawals = oneCurrency
	if !a.availableBalance().IsZero() {
		t.Fatal("unexpected available host balance")
	}
}

// testAccountManagedCommitDeposit is a small unit test that verifies the
// functionality of the managedCommitDeposit method on the account
func testAccountManagedCommitDeposit(t *testing.T) {
	t.Parallel()

	oneCurrency := types.NewCurrency64(1)

	// base case
	a := new(account)
	a.pendingDeposits = oneCurrency
	a.managedCommitDeposit(oneCurrency, true)

	// assert deposit was added
	if !a.negativeBalance.IsZero() {
		t.Fatal("unexpected negative balance", a.negativeBalance)
	}
	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.Equals(oneCurrency) {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingDeposits.IsZero() {
		t.Fatal("unexpected pending deposits", a.pendingDeposits)
	}

	// failure case
	a.pendingDeposits = oneCurrency
	a.managedCommitDeposit(oneCurrency, false)

	// assert balances are left unchanged and pending deposits is 0
	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.Equals(oneCurrency) {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingDeposits.IsZero() {
		t.Fatal("unexpected pending deposits", a.pendingDeposits)
	}

	// negative balance case (no host neg balance)
	a.pendingDeposits = oneCurrency
	a.negativeBalance = oneCurrency
	a.managedCommitDeposit(oneCurrency, true)

	// assert (host) deposit was added, and account balance is unchanged and negative balance was adjusted
	if !a.negativeBalance.IsZero() {
		t.Fatal("unexpected negative balance", a.negativeBalance)
	}
	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.Equals(oneCurrency.Mul64(2)) {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingDeposits.IsZero() {
		t.Fatal("unexpected pending deposits", a.pendingDeposits)
	}

	// negative balance case (with host neg balance)
	a.pendingDeposits = oneCurrency
	a.negativeBalance = oneCurrency
	a.hostBalanceNegative = oneCurrency
	a.managedCommitDeposit(oneCurrency, true)

	// assert deposit was not added, and only negative account balances were adjusted
	if !a.negativeBalance.IsZero() {
		t.Fatal("unexpected negative balance", a.negativeBalance)
	}
	if !a.hostBalanceNegative.IsZero() {
		t.Fatal("unexpected host negative balance", a.hostBalanceNegative)
	}
	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.Equals(oneCurrency.Mul64(2)) {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingDeposits.IsZero() {
		t.Fatal("unexpected pending deposits", a.pendingDeposits)
	}
}

// testAccountManagedCommitWithdrawal is a small unit test that verifies the
// functionality of the managedCommitWithdrawal method on the account
func testAccountManagedCommitWithdrawal(t *testing.T) {
	t.Parallel()

	// create a temporary accounts file
	deps := modules.ProdDependencies
	file, err := deps.OpenFile(filepath.Join(t.TempDir(), accountsFilename), os.O_RDWR|os.O_CREATE, defaultFilePerm)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err = file.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// create discard logger
	logger, err := persist.NewLogger(ioutil.Discard)
	if err != nil {
		t.Fatal(err)
	}

	// ensure our dummy account has everything it needs to process a withdrawal
	a := new(account)
	a.spending = spendingDetails{}
	a.staticAlerter = skymodules.NewAlerter("renter")
	a.staticBalanceTarget = types.SiacoinPrecision
	a.staticFile = file
	a.staticLog = logger

	oneCurrency := types.NewCurrency64(1)

	// base case
	a.balance = oneCurrency
	a.hostBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency.Mul64(2) // 1H refund
	a.managedCommitWithdrawal(categoryDownload, oneCurrency, oneCurrency, nil)

	// assert withdrawal was processed
	if !a.balance.IsZero() {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.IsZero() {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingWithdrawals.IsZero() {
		t.Fatal("unexpected pending withdrawals", a.pendingWithdrawals)
	}

	// failure case
	a.balance = oneCurrency
	a.hostBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency.Mul64(2) // 1H refund
	a.managedCommitWithdrawal(categoryDownload, oneCurrency, oneCurrency, errors.New("failed"))

	// assert balances remained the same but pending withdrawals got updated
	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.hostBalance.Equals(oneCurrency) {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.pendingWithdrawals.IsZero() {
		t.Fatal("unexpected pending withdrawals", a.pendingWithdrawals)
	}

	// negative balances case
	a.pendingWithdrawals = oneCurrency.Mul64(3) // 2H withdrawal + 1H refund
	a.managedCommitWithdrawal(categoryDownload, oneCurrency.Mul64(2), oneCurrency, nil)

	// assert negative balances were updated
	if !a.balance.IsZero() {
		t.Fatal("unexpected balance", a.balance)
	}
	if !a.negativeBalance.Equals(oneCurrency) {
		t.Fatal("unexpected negative balance", a.negativeBalance)
	}
	if !a.hostBalance.IsZero() {
		t.Fatal("unexpected host balance", a.hostBalance)
	}
	if !a.hostBalanceNegative.Equals(oneCurrency) {
		t.Fatal("unexpected negative host balance", a.hostBalanceNegative)
	}
	if !a.pendingWithdrawals.IsZero() {
		t.Fatal("unexpected pending withdrawals", a.pendingWithdrawals)
	}

	// alert case
	a.balance = types.SiacoinPrecision
	a.pendingWithdrawals = oneCurrency
	a.managedCommitWithdrawal(categoryDownload, oneCurrency, types.ZeroCurrency, host.ErrBalanceInsufficient)

	// define a helper function that verifies whether the alert was registered
	isBalanceAlertRegistered := func() (registered bool) {
		_, errs, _, _ := a.staticAlerter.Alerts()
		for _, alert := range errs {
			if strings.Contains(alert.Msg, "the ephemeral account is out of balance") {
				registered = true
			}
		}
		return
	}

	// assert the alert was registered
	if !isBalanceAlertRegistered() {
		t.Fatal("expected")
	}

	// commit a successful withdrawal and assert it got unregistered
	a.balance = oneCurrency
	a.hostBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency
	a.managedCommitWithdrawal(categoryDownload, oneCurrency, types.ZeroCurrency, nil)

	// assert the alert was unregistered
	if isBalanceAlertRegistered() {
		t.Fatal("expected")
	}
}

// testAccountMinAndMaxExpectedBalance is a small unit test that verifies the
// functionality of the min and max expected balance functions.
func testAccountMinAndMaxExpectedBalance(t *testing.T) {
	t.Parallel()

	oneCurrency := types.NewCurrency64(1)

	a := new(account)
	a.balance = oneCurrency
	a.negativeBalance = oneCurrency
	if !a.minExpectedBalance().Equals(types.ZeroCurrency) {
		t.Fatal("unexpected min expected balance")
	}
	if !a.maxExpectedBalance().Equals(types.ZeroCurrency) {
		t.Fatal("unexpected max expected balance")
	}

	a = new(account)
	a.balance = oneCurrency.Mul64(2)
	a.negativeBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency
	if !a.minExpectedBalance().Equals(types.ZeroCurrency) {
		t.Fatal("unexpected min expected balance")
	}
	if !a.maxExpectedBalance().Equals(oneCurrency) {
		t.Fatal("unexpected max expected balance")
	}

	a = new(account)
	a.balance = oneCurrency.Mul64(3)
	a.negativeBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency
	if !a.minExpectedBalance().Equals(oneCurrency) {
		t.Fatal("unexpected min expected balance")
	}
	if !a.maxExpectedBalance().Equals(oneCurrency.Mul64(2)) {
		t.Fatal("unexpected max expected balance")
	}

	a = new(account)
	a.balance = oneCurrency.Mul64(3)
	a.negativeBalance = oneCurrency
	a.pendingWithdrawals = oneCurrency
	a.pendingDeposits = oneCurrency
	if !a.minExpectedBalance().Equals(oneCurrency) {
		t.Fatal("unexpected min expected balance")
	}
	if !a.maxExpectedBalance().Equals(oneCurrency.Mul64(3)) {
		t.Fatal("unexpected max expected balance")
	}
}

// testAccountSyncBalance is a small unit test that verifies the functionality
// of the sync balance function.
func testAccountSyncBalance(t *testing.T) {
	t.Parallel()

	// create a mock of the accounts file
	deps := modules.ProdDependencies
	f, err := deps.OpenFile(filepath.Join(t.TempDir(), accountsFilename), os.O_RDWR|os.O_CREATE, defaultFilePerm)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err = f.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	oneCurrency := types.NewCurrency64(1)

	a := new(account)
	a.staticFile = f
	a.balance = types.ZeroCurrency
	a.negativeBalance = oneCurrency
	a.pendingDeposits = oneCurrency
	a.pendingWithdrawals = oneCurrency
	a.managedSyncBalance(oneCurrency, false)

	if !a.balance.Equals(oneCurrency) {
		t.Fatal("unexpected balance after reset", a.balance)
	}
	if !a.negativeBalance.IsZero() {
		t.Fatal("unexpected negative balance after reset", a.negativeBalance)
	}
	if !a.pendingDeposits.IsZero() {
		t.Fatal("unexpected pending deposits after reset", a.pendingDeposits)
	}
	if !a.pendingWithdrawals.IsZero() {
		t.Fatal("unexpected pending withdrawals after reset", a.pendingWithdrawals)
	}
	if !a.balanceDriftPositive.Equals(oneCurrency) || !a.balanceDriftNegative.IsZero() {
		t.Fatal("unexpected drift")
	}
	if a.syncAt == (time.Time{}) {
		t.Fatal("unexpected sync at")
	}

	// verify negative drift gets updated properly as well
	a.managedSyncBalance(types.ZeroCurrency, false)
	if !a.balanceDriftPositive.Equals(oneCurrency) || !a.balanceDriftNegative.Equals(oneCurrency) {
		t.Fatal("unexpected drift")
	}
}

// testAccountResetSpending is a small unit test that verifies the functionality
// of the method 'resetSpending' on the account
func testAccountResetSpending(t *testing.T) {
	t.Parallel()

	// create a mock of the accounts file
	deps := modules.ProdDependencies
	f, err := deps.OpenFile(filepath.Join(t.TempDir(), accountsFilename), os.O_RDWR|os.O_CREATE, defaultFilePerm)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err = f.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// create an account
	a := new(account)
	a.balance = types.SiacoinPrecision
	a.spending.downloads = types.SiacoinPrecision.Mul64(2)
	a.staticFile = f

	// assert balance is non zero and residue is zero
	if a.balance.IsZero() {
		t.Fatalf("unexpected balance, %v == 0", a.balance.HumanString())
	}
	if !a.residue.IsZero() {
		t.Fatalf("unexpected residue, %v != 0", a.residue.HumanString())
	}

	// reset spending
	a.managedResetSpending()

	// assert reset state
	if !a.spending.sum().IsZero() {
		t.Fatal("unexpected")
	}
	if !a.residue.Equals(a.balance) {
		t.Fatalf("unexpected residue, %v != %v", a.residue.HumanString(), a.balance.HumanString())
	}
}

// testAccountTrackSpending is a small unit test that verifies the functionality
// of the method 'trackSpending' on the account
func testAccountTrackSpending(t *testing.T) {
	t.Parallel()

	// create a mock of the accounts file, tracking a spend needs a file to
	// write to
	deps := modules.ProdDependencies
	f, err := deps.OpenFile(filepath.Join(t.TempDir(), accountsFilename), os.O_RDWR|os.O_CREATE, defaultFilePerm)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err = f.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// create an account
	a := new(account)
	a.staticFile = f

	// verify initial state
	hasting := types.NewCurrency64(1)
	if !a.spending.accountBalance.IsZero() ||
		!a.spending.downloads.IsZero() ||
		!a.spending.snapshotDownloads.IsZero() ||
		!a.spending.registryReads.IsZero() ||
		!a.spending.registryWrites.IsZero() ||
		!a.spending.repairDownloads.IsZero() ||
		!a.spending.repairUploads.IsZero() ||
		!a.spending.subscriptions.IsZero() ||
		!a.spending.updatePriceTable.IsZero() ||
		!a.spending.uploads.IsZero() {
		t.Fatal("unexpected")
	}

	// verify every category tracks its own field
	a.trackSpending(categoryAccountBalance, hasting.Mul64(1), false)
	a.trackSpending(categoryDownload, hasting.Mul64(2), false)
	a.trackSpending(categorySnapshotDownload, hasting.Mul64(3), false)
	a.trackSpending(categorySnapshotUpload, hasting.Mul64(4), false)
	a.trackSpending(categoryRegistryRead, hasting.Mul64(5), false)
	a.trackSpending(categoryRegistryWrite, hasting.Mul64(6), false)
	a.trackSpending(categoryRepairDownload, hasting.Mul64(7), false)
	a.trackSpending(categoryRepairUpload, hasting.Mul64(8), false)
	a.trackSpending(categorySubscription, hasting.Mul64(9), false)
	a.trackSpending(categoryUpdatePriceTable, hasting.Mul64(10), false)
	a.trackSpending(categoryUpload, hasting.Mul64(11), false)

	if !a.spending.accountBalance.Equals(hasting.Mul64(1)) ||
		!a.spending.downloads.Equals(hasting.Mul64(2)) ||
		!a.spending.snapshotDownloads.Equals(hasting.Mul64(3)) ||
		!a.spending.snapshotUploads.Equals(hasting.Mul64(4)) ||
		!a.spending.registryReads.Equals(hasting.Mul64(5)) ||
		!a.spending.registryWrites.Equals(hasting.Mul64(6)) ||
		!a.spending.repairDownloads.Equals(hasting.Mul64(7)) ||
		!a.spending.repairUploads.Equals(hasting.Mul64(8)) ||
		!a.spending.subscriptions.Equals(hasting.Mul64(9)) ||
		!a.spending.updatePriceTable.Equals(hasting.Mul64(10)) ||
		!a.spending.uploads.Equals(hasting.Mul64(11)) {
		t.Fatal("unexpected")
	}

	// verify refunds are tracked properly
	a.trackSpending(categoryAccountBalance, hasting.Mul64(1), true)
	a.trackSpending(categoryDownload, hasting.Mul64(2), true)
	a.trackSpending(categorySnapshotDownload, hasting.Mul64(3), true)
	a.trackSpending(categorySnapshotUpload, hasting.Mul64(4), true)
	a.trackSpending(categoryRegistryRead, hasting.Mul64(5), true)
	a.trackSpending(categoryRegistryWrite, hasting.Mul64(6), true)
	a.trackSpending(categoryRepairDownload, hasting.Mul64(7), true)
	a.trackSpending(categoryRepairUpload, hasting.Mul64(8), true)
	a.trackSpending(categorySubscription, hasting.Mul64(9), true)
	a.trackSpending(categoryUpdatePriceTable, hasting.Mul64(10), true)
	a.trackSpending(categoryUpload, hasting.Mul64(11), true)
	if !a.spending.sum().IsZero() {
		t.Fatal("unexpected")
	}

	// check category sanity check
	func() {
		defer func() {
			if r := recover(); r == nil || !strings.Contains(fmt.Sprintf("%v", r), "uninitialized category") {
				t.Fatalf("expected panic when attempting to track a spend where the category is not initialised, instead we recovered %v", r)
			}
		}()
		a.trackSpending(categoryErr, hasting, false)
	}()

	// check category sanity check
	func() {
		defer func() {
			if r := recover(); r == nil || !strings.Contains(fmt.Sprintf("%v", r), "category is not handled, developer error") {
				t.Fatalf("expected panic when attempting to track a spend where the category is unknown, instead we recovered %v", r)
			}
		}()
		a.trackSpending(spendingCategory(math.MaxUint64), hasting, false)
	}()
}

// testAccountCreation verifies newAccount returns a valid account object
func testAccountCreation(t *testing.T, rt *renterTester) {
	r := rt.renter

	// create a random hostKey
	_, pk := crypto.GenerateKeyPair()
	hostKey := types.SiaPublicKey{
		Algorithm: types.SignatureEd25519,
		Key:       pk[:],
	}

	// create an account with a different hostkey to ensure the account we are
	// going to validate has an offset different from 0
	tmpKey, _ := newRandomHostKey()
	_, err := r.staticAccountManager.managedOpenAccount(tmpKey)
	if err != nil {
		t.Fatal(err)
	}

	// create a new account object
	account, err := r.staticAccountManager.managedOpenAccount(hostKey)
	if err != nil {
		t.Fatal(err)
	}

	// validate the account object
	if account.staticID.IsZeroAccount() {
		t.Fatal("Invalid account ID")
	}
	if account.staticOffset == 0 {
		t.Fatal("Invalid offset")
	}
	if !account.staticHostKey.Equals(hostKey) {
		t.Fatal("Invalid host key")
	}

	// validate the account id is built using a valid SiaPublicKey and the
	// account's secret key belongs to the public key used to construct the id
	hash := crypto.HashBytes(fastrand.Bytes(10))
	sig := crypto.SignHash(hash, account.staticSecretKey)
	err = crypto.VerifyHash(hash, account.staticID.SPK().ToPublicKey(), sig)
	if err != nil {
		t.Fatal("Invalid secret key")
	}
}

// testAccountTracking unit tests all of the methods on the account that track
// deposits or withdrawals.
func testAccountTracking(t *testing.T, rt *renterTester) {
	r := rt.renter

	// create a random account
	hostKey, _ := newRandomHostKey()
	account, err := r.staticAccountManager.managedOpenAccount(hostKey)
	if err != nil {
		t.Fatal(err)
	}

	// verify tracking a deposit properly alters the account state
	deposit := types.SiacoinPrecision
	account.managedTrackDeposit(deposit)
	if !account.pendingDeposits.Equals(deposit) {
		t.Log(account.pendingDeposits)
		t.Fatal("Tracking a deposit did not properly alter the account's state")
	}

	// verify committing a deposit decrements the pendingDeposits and properly
	// adjusts the account balance depending on whether success is true or false
	account.managedCommitDeposit(deposit, false)
	if !account.pendingDeposits.IsZero() {
		t.Fatal("Committing a deposit did not properly alter the  account's state")
	}
	if !account.balance.IsZero() {
		t.Fatal("Committing a failed deposit wrongfully adjusted the account balance")
	}
	account.managedTrackDeposit(deposit) // redo the deposit
	account.managedCommitDeposit(deposit, true)
	if !account.pendingDeposits.IsZero() {
		t.Fatal("Committing a deposit did not properly alter the  account's state")
	}
	if !account.balance.Equals(deposit) {
		t.Fatal("Committing a successful deposit wrongfully adjusted the account balance")
	}

	// verify tracking a withdrawal properly alters the account state
	withdrawal := types.SiacoinPrecision.Div64(100)
	account.managedTrackWithdrawal(withdrawal)
	if !account.pendingWithdrawals.Equals(withdrawal) {
		t.Log(account.pendingWithdrawals)
		t.Fatal("Tracking a withdrawal did not properly alter the account's state")
	}

	// verify committing a withdrawal decrements the pendingWithdrawals and
	// properly adjusts the account balance depending on whether success is true
	// or false
	account.managedCommitWithdrawal(categoryUpload, withdrawal, types.ZeroCurrency, errors.New("failed"))
	if !account.pendingWithdrawals.IsZero() {
		t.Fatal("Committing a withdrawal did not properly alter the account's state")
	}
	if !account.balance.Equals(deposit) {
		t.Fatal("Committing a failed withdrawal wrongfully adjusted the account balance")
	}
	account.managedTrackWithdrawal(withdrawal) // redo the withdrawal
	account.managedCommitWithdrawal(categoryUpload, withdrawal, types.ZeroCurrency, nil)
	if !account.pendingWithdrawals.IsZero() {
		t.Fatal("Committing a withdrawal did not properly alter the account's state")
	}
	if !account.balance.Equals(deposit.Sub(withdrawal)) {
		t.Fatal("Committing a successful withdrawal wrongfully adjusted the account balance")
	}

	// verify committing a successful withdrawal with a spending category
	// tracks the spend in the spending details, we only verify one category
	// here but the others are covered in the unit test 'trackSpending'
	account.managedTrackWithdrawal(withdrawal)
	account.managedCommitWithdrawal(categoryDownload, withdrawal, types.ZeroCurrency, nil)
	if !account.spending.downloads.Equals(withdrawal) {
		t.Fatal("Committing a successful withdrawal with a valid spending category should have update the appropriate field in the spending details")
	}
}

// testAccountClosed verifies accounts can not be opened after the 'closed' flag
// has been set to true by the save.
func testAccountClosed(t *testing.T, closedRenter *Renter) {
	hk, _ := newRandomHostKey()
	_, err := closedRenter.staticAccountManager.managedOpenAccount(hk)
	if !strings.Contains(err.Error(), "file already closed") {
		t.Fatal("Unexpected error when opening an account, err:", err)
	}
}

// testAccountCriticalOnDoubleSave verifies the critical when
// managedSaveAccounts is called twice.
func testAccountCriticalOnDoubleSave(t *testing.T, closedRenter *Renter) {
	defer func() {
		if r := recover(); r != nil {
			err := fmt.Sprint(r)
			if !strings.Contains(err, "Trying to save accounts twice") {
				t.Fatal("Expected error not returned")
			}
		}
	}()
	err := closedRenter.staticAccountManager.managedSaveAndClose()
	if err == nil {
		t.Fatal("Expected build.Critical on double save")
	}
}

// testWorkerAccountHostAccountBalance verifies the functionality of
// staticHostAccountBalance that performs the account balance RPC on the host
func testWorkerAccountHostAccountBalance(t *testing.T, wt *workerTester) {
	w := wt.worker

	// fetch the host account balance and assert it's correct
	balance, err := w.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}
	if balance.IsZero() || !balance.Equals(w.staticAccount.managedAvailableBalance()) {
		t.Fatalf("unexpected balance, %v != %v", balance, w.staticAccount.managedAvailableBalance())
	}
}

// testWorkerAccountSyncAccountBalanceToHostCritical is a small unit test that
// verifies the sync can not be called when the account delta is not zero
func testWorkerAccountSyncAccountBalanceToHostCritical(t *testing.T, wt *workerTester) {
	w := wt.worker
	r := w.staticRenter

	// track a deposit to simulate an ongoing fund
	w.staticAccount.managedTrackDeposit(r.staticAccountBalanceTarget)

	// trigger the account balance sync and expect it to panic
	defer func() {
		r := recover()
		if r == nil || !strings.Contains(fmt.Sprintf("%v", r), "managedSyncAccountBalanceToHost is called on a worker with an account that has non-zero deltas") {
			t.Error("Expected build.Critical")
			t.Log(r)
		}
	}()

	w.externSyncAccountBalanceToHost(false)
}

// testWorkerAccountSpendingDetails verifies that performing actions such as
// downloading and reading, writing and subscribing to the registry properly
// update the spending details in the worker account.
func testWorkerAccountSpendingDetails(t *testing.T, wt *workerTester) {
	w := wt.worker

	// verify initial state
	spending := w.staticAccount.callSpendingDetails()
	if !spending.downloads.IsZero() ||
		!spending.registryReads.IsZero() ||
		!spending.registryWrites.IsZero() ||
		!spending.repairDownloads.IsZero() ||
		!spending.repairUploads.IsZero() ||
		!spending.snapshotDownloads.IsZero() ||
		!spending.snapshotUploads.IsZero() ||
		!spending.subscriptions.IsZero() {
		t.Fatal("unexpected")
	}

	// create a registry value
	rv, spk, sk := randomRegistryValue()

	// update the registry
	err := wt.UpdateRegistry(context.Background(), spk, rv)
	if err != nil {
		t.Fatal(err)
	}

	spending = w.staticAccount.callSpendingDetails()
	if spending.registryWrites.IsZero() {
		t.Fatal("unexpected")
	}

	// read from the registry
	_, err = wt.ReadRegistry(context.Background(), testSpan(), spk, rv.Tweak)
	if err != nil {
		t.Fatal(err)
	}

	spending = w.staticAccount.callSpendingDetails()
	if spending.registryReads.IsZero() {
		t.Fatal("unexpected")
	}

	// upload a snapshot to fill the first sector of the contract.
	backup := skymodules.UploadedBackup{
		Name:           "foo",
		CreationDate:   types.CurrentTimestamp(),
		Size:           10,
		UploadProgress: 0,
	}
	err = wt.UploadSnapshot(context.Background(), backup, fastrand.Bytes(int(backup.Size)))
	if err != nil {
		t.Fatal(err)
	}

	// download snapshot
	_, err = wt.DownloadSnapshotTable(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	spending = w.staticAccount.callSpendingDetails()
	if spending.snapshotDownloads.IsZero() {
		t.Fatal("unexpected")
	}

	// verify executing a program with specifying the spending category results
	// in an update to the spending details for that category
	pt := wt.staticPriceTable().staticPriceTable
	pb := modules.NewProgramBuilder(&pt, 0)
	pb.AddHasSectorInstruction(crypto.Hash{})
	p, data := pb.Program()
	cost, _, _ := pb.Cost(true)

	jhs := new(jobHasSector)
	jhs.staticSectors = []crypto.Hash{{1, 2, 3}}
	ulBandwidth, dlBandwidth := jhs.callExpectedBandwidth()
	bandwidthCost, bandwidthRefund := mdmBandwidthCost(pt, ulBandwidth, dlBandwidth)
	cost = cost.Add(bandwidthCost)

	// execute it
	_, _, err = w.managedExecuteProgram(p, data, types.FileContractID{}, categoryDownload, cost, bandwidthRefund)
	if err != nil {
		t.Fatal(err)
	}

	spending = w.staticAccount.callSpendingDetails()
	if spending.downloads.IsZero() {
		t.Fatal("unexpected")
	}

	// Subscribe to the random registry value we created earlier.
	// Subscribe to that entry.
	req := skymodules.SubscriptionRequest{
		EntryID: modules.DeriveRegistryEntryID(spk, rv.Tweak),
		PubKey:  &spk,
		Tweak:   &rv.Tweak,
	}

	// Subscribe
	wt.UpdateSubscriptions(req)

	// Wait for first notification.
	subs := wt.staticSubscriptionInfo.subscriptions
	sub, exist := subs[modules.DeriveRegistryEntryID(spk, rv.Tweak)]
	if !exist {
		t.Fatal("subscribed to entry doesn't have a subscription")
	}
	select {
	case <-time.After(time.Minute):
		t.Fatal("timeout")
	case <-sub.subscribed:
	}

	// Update the entry on the host.
	rv.Revision++
	rv = rv.Sign(sk)
	err = wt.UpdateRegistry(context.Background(), spk, rv)
	if err != nil {
		t.Fatal(err)
	}

	// Unsubscribe
	wt.UpdateSubscriptions()

	// Stop the loop by shutting down the worker.
	err = wt.staticTG.Stop()
	if err != nil {
		t.Fatal(err)
	}

	// Check the spending is updated
	time.Sleep(stopSubscriptionGracePeriod)
	spending = w.staticAccount.callSpendingDetails()
	if spending.subscriptions.IsZero() {
		t.Fatal("unexpected")
	}

	// Reload thhe renter and reload the account for that same host
	hostkey := wt.worker.staticHostPubKey
	r, err := wt.rt.reloadRenter(wt.staticRenter)
	if err != nil {
		t.Fatal(err)
	}
	account, err := r.staticAccountManager.managedOpenAccount(hostkey)
	if err != nil {
		t.Fatal(err)
	}

	// Except the account's spending details were reloaded properly. We do not
	// strictly compare but verify it's at least equal to what it was prior to
	// shutdown to avoid NDFs.
	reloaded := account.callSpendingDetails()
	if reloaded.downloads.Cmp(spending.downloads) < 0 ||
		reloaded.registryReads.Cmp(spending.registryReads) < 0 ||
		reloaded.registryWrites.Cmp(spending.registryWrites) < 0 ||
		reloaded.repairDownloads.Cmp(spending.repairDownloads) < 0 ||
		reloaded.repairUploads.Cmp(spending.repairUploads) < 0 ||
		reloaded.snapshotDownloads.Cmp(spending.snapshotDownloads) < 0 ||
		reloaded.snapshotUploads.Cmp(spending.snapshotUploads) < 0 ||
		reloaded.subscriptions.Cmp(spending.subscriptions) < 0 {
		t.Fatal("unexpected")
	}
}

// TestNewWithdrawalMessage verifies the newWithdrawalMessage helper
// properly instantiates all required fields on the WithdrawalMessage
func TestNewWithdrawalMessage(t *testing.T) {
	t.Parallel()
	// create a withdrawal message using random parameters
	aid, _ := modules.NewAccountID()
	amount := types.NewCurrency64(fastrand.Uint64n(100))
	blockHeight := types.BlockHeight(fastrand.Intn(100))
	msg := newWithdrawalMessage(aid, amount, blockHeight)

	// validate the withdrawal message
	if msg.Account != aid {
		t.Fatal("Unexpected account ID")
	}
	if !msg.Amount.Equals(amount) {
		t.Fatal("Unexpected amount")
	}
	if msg.Expiry != blockHeight+withdrawalValidityPeriod {
		t.Fatal("Unexpected expiry")
	}
	if len(msg.Nonce) != modules.WithdrawalNonceSize {
		t.Fatal("Unexpected nonce length")
	}
	var nonce [modules.WithdrawalNonceSize]byte
	if bytes.Equal(msg.Nonce[:], nonce[:]) {
		t.Fatal("Uninitialized nonce")
	}
}

// TestWorkerAccountHostAccountBalancePayment verifies the host balance RPC is
// paid for using the EA if possible
func TestWorkerAccountHostAccountBalancePayment(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// create a new worker tester with a disabled worker
	wt, err := newWorkerTesterCustomDependency(t.Name(), &dependencies.DependencyDisableWorker{}, modules.ProdDependencies)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		err := wt.Close()
		if err != nil {
			t.Fatal(err)
		}
	}()

	// fetch the maintenance spending before doing anything
	financials, err := wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}

	// assert we've spent 0 money from our contract on fetching the account balance
	if !financials.MaintenanceSpending.AccountBalanceCost.IsZero() {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.AccountBalanceCost)
	}

	// fetch a price table and refill the account manually
	wt.staticUpdatePriceTable()

	// fetch the account balance PT cost
	pt := wt.worker.staticPriceTable()
	fetchBalanceCost := pt.staticPriceTable.AccountBalanceCost
	balanceTarget := wt.worker.staticRenter.staticAccountBalanceTarget

	// assert the worker's maintenance is OK
	wt.worker.staticMaintenanceState.accountRefillSucceeded = true
	wt.worker.staticMaintenanceState.accountSyncSucceeded = true
	wt.worker.staticMaintenanceState.revisionsMismatchFixSucceeded = true
	if !wt.worker.managedMaintenanceSucceeded() {
		t.Fatal("unexpected maintenance status")
	}

	// fetch the host balance
	_, err = wt.worker.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}

	// assert the RPC was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.AccountBalanceCost.Equals(fetchBalanceCost) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.AccountBalanceCost)
	}

	// refill the account
	wt.managedRefillAccount()

	// fetch the current EA balance and assert it's full
	balance := wt.worker.staticAccount.managedAvailableBalance()
	if !balance.Equals(balanceTarget) {
		t.Fatal("unexpected balance", balance)
	}

	// fetch the host balance
	_, err = wt.worker.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}

	// assert the RPC was paid for by EA
	if !wt.worker.staticAccount.managedAvailableBalance().Add(fetchBalanceCost).Equals(balanceTarget) {
		t.Fatal("unexpected account balance")
	}

	// mock a worker's maintenance failure
	wt.worker.staticMaintenanceState.priceTableUpdateSucceeded = false

	// fetch the host balance
	_, err = wt.worker.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}

	// assert the RPC was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.AccountBalanceCost.Equals(fetchBalanceCost.Mul64(2)) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.AccountBalanceCost)
	}

	// reinstate the maintenance
	wt.worker.staticMaintenanceState.priceTableUpdateSucceeded = true
	if !wt.worker.managedMaintenanceSucceeded() {
		t.Fatal("unexpected maintenance status")
	}

	// fetch the host balance
	_, err = wt.worker.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}

	// assert the RPC was paid for by EA
	if !wt.worker.staticAccount.managedAvailableBalance().Add(fetchBalanceCost.Mul64(2)).Equals(balanceTarget) {
		t.Fatal("unexpected account balance")
	}

	// set the account balance to below the pay by ea threshold
	threshold := balanceTarget.MulFloat(maintenancePayByEAPercentageThreshold)
	wt.worker.staticAccount.balance = threshold.Sub64(1)

	// fetch the host balance
	_, err = wt.worker.managedHostAccountBalance()
	if err != nil {
		t.Fatal(err)
	}

	// assert the RPC was paid for by contract
	financials, err = wt.rt.renter.PeriodSpending()
	if err != nil {
		t.Fatal(err)
	}
	if !financials.MaintenanceSpending.AccountBalanceCost.Equals(fetchBalanceCost.Mul64(3)) {
		t.Fatal("unexpected financials", financials.MaintenanceSpending.AccountBalanceCost)
	}
}

// openRandomTestAccountsOnRenter is a helper function that creates a random
// number of accounts by calling 'managedOpenAccount' on the given renter
func openRandomTestAccountsOnRenter(r *Renter) ([]*account, error) {
	// randomBalance is a small helper function that returns a random
	// types.Currency taking into account the given max value
	randomBalance := func(max uint64) types.Currency {
		return types.NewCurrency64(fastrand.Uint64n(max)).Add(types.NewCurrency64(1))
	}

	var accounts []*account
	for i := 0; i < fastrand.Intn(10)+1; i++ {
		hostKey := types.SiaPublicKey{
			Algorithm: types.SignatureEd25519,
			Key:       fastrand.Bytes(crypto.PublicKeySize),
		}
		account, err := r.staticAccountManager.managedOpenAccount(hostKey)
		if err != nil {
			return nil, err
		}

		// give it a random balance state
		account.balance = randomBalance(1e3)
		account.negativeBalance = randomBalance(1e2)
		account.pendingDeposits = randomBalance(1e2)
		account.pendingWithdrawals = randomBalance(1e2)
		account.spending = spendingDetails{
			accountBalance:    randomBalance(1e1),
			downloads:         randomBalance(1e1),
			registryReads:     randomBalance(1e1),
			registryWrites:    randomBalance(1e1),
			repairDownloads:   randomBalance(1e1),
			repairUploads:     randomBalance(1e1),
			snapshotDownloads: randomBalance(1e1),
			snapshotUploads:   randomBalance(1e1),
			subscriptions:     randomBalance(1e1),
			updatePriceTable:  randomBalance(1e1),
			uploads:           randomBalance(1e1),
		}
		account.residue = types.SiacoinPrecision
		account.hostBalance = account.pendingWithdrawals.Add(randomBalance(1e2)) // ensure it's greater than pending withdrawals to avoid zero currency on persist
		account.hostBalanceNegative = randomBalance(1e2)
		accounts = append(accounts, account)
	}
	return accounts, nil
}
