// Package server provides a server that can wrap a node and serve an http api
// for interacting with the node.
package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"syscall"
	"time"

	mnemonics "gitlab.com/NebulousLabs/entropy-mnemonics"
	"gitlab.com/NebulousLabs/errors"

	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/node"
	"gitlab.com/SkynetLabs/skyd/node/api"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/types"
)

const (
	// StackTracesFolderName is the name of the folder to which stack traces are
	// saved when the server takes more than stackDumpTimeout to
	// shut down.
	StackTracesFolderName = "stacktraces"

	// logFileName is the name of the API's log file.
	logFileName = "api.log"
)

var (
	// stackDumpTimeout is the amount of time we wait before dumping the stack
	// on renter shutdown.
	stackDumpTimeout = build.Select(build.Var{
		Dev:      time.Minute,
		Standard: time.Minute,
		Testing:  3 * time.Second,
	}).(time.Duration)
)

// A Server is a collection of siad modules that can be communicated with over
// an http api.
type Server struct {
	api               *api.API
	apiServer         *http.Server
	listener          net.Listener
	logFile           *os.File
	node              *node.Node
	requiredUserAgent string
	Dir               string

	serveChan chan struct{}
	serveErr  error

	closeOnce  sync.Once
	staticDeps modules.Dependencies
}

// serve listens for and handles API calls. It is a blocking function.
func (srv *Server) serve() error {
	// The server will run until an error is encountered or the listener is
	// closed, via either the Close method or by signal handling.  Closing the
	// listener will result in the benign error handled below.
	err := srv.apiServer.Serve(srv.listener)
	if err != nil && !strings.HasSuffix(err.Error(), "use of closed network connection") && !errors.Contains(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

// Close closes the Server's listener, causing the HTTP server to shut down.
func (srv *Server) Close() error {
	var err error
	srv.closeOnce.Do(func() {
		doneChan := make(chan struct{})
		defer close(doneChan)

		// Dump the stack trace if closing the server takes too long
		go func() {
			select {
			case <-time.After(stackDumpTimeout):
				if err := srv.DumpStackTrace(); err != nil {
					os.Stderr.WriteString(err.Error())
				}
			case <-doneChan:
			}
		}()

		// Disrupt triggers a sleep to ensure the stack gets dumped
		if srv.staticDeps.Disrupt("SlowServerShutdown") {
			time.Sleep(stackDumpTimeout)
		}

		// Stop accepting API requests.
		err := srv.apiServer.Shutdown(context.Background())

		// Wait for serve() to return and capture its error.
		<-srv.serveChan
		if !errors.Contains(srv.serveErr, http.ErrServerClosed) {
			err = errors.Compose(err, srv.serveErr)
		}

		// Shutdown skymodules.
		if srv.node != nil {
			err = errors.Compose(err, srv.node.Close())
		}

		// Close logfile
		err = errors.Compose(err, srv.logFile.Close())
		err = errors.AddContext(err, "error while closing server")
	})
	return err
}

// APIAddress returns the underlying node's api address
func (srv *Server) APIAddress() string {
	return srv.listener.Addr().String()
}

// GatewayAddress returns the underlying node's gateway address
func (srv *Server) GatewayAddress() modules.NetAddress {
	return srv.node.Gateway.Address()
}

// HostPublicKey returns the host's public key or an error if the node has no
// host.
func (srv *Server) HostPublicKey() (types.SiaPublicKey, error) {
	if srv.node.Host == nil {
		return types.SiaPublicKey{}, errors.New("can't get public host key of a non-host node")
	}
	return srv.node.Host.PublicKey(), nil
}

// RenterCurrentPeriod returns the renter's current period or an error if the
// node has no renter
func (srv *Server) RenterCurrentPeriod() (types.BlockHeight, error) {
	if srv.node.Renter == nil {
		return 0, errors.New("can't get renter settings for a non-renter node")
	}
	return srv.node.Renter.CurrentPeriod(), nil
}

// RenterSettings returns the renter's settings or an error if the node has no
// renter
func (srv *Server) RenterSettings() (skymodules.RenterSettings, error) {
	if srv.node.Renter == nil {
		return skymodules.RenterSettings{}, errors.New("can't get renter settings for a non-renter node")
	}
	return srv.node.Renter.Settings()
}

// ServeErr returns an error channel that contains the serve error when the
// channel closes.
func (srv *Server) ServeErr() <-chan error {
	errorChan := make(chan error, 1)
	go func() {
		<-srv.serveChan
		errorChan <- srv.serveErr
		close(errorChan)
	}()
	return errorChan
}

// Unlock unlocks the server's wallet using the provided password.
func (srv *Server) Unlock(password string) error {
	if srv.node.Wallet == nil {
		return errors.New("server doesn't have a wallet")
	}
	var validKeys []crypto.CipherKey
	dicts := []mnemonics.DictionaryID{"english", "german", "japanese"}
	for _, dict := range dicts {
		seed, err := modules.StringToSeed(password, dict)
		if err != nil {
			continue
		}
		validKeys = append(validKeys, crypto.NewWalletKey(crypto.HashObject(seed)))
	}
	validKeys = append(validKeys, crypto.NewWalletKey(crypto.HashObject(password)))
	for _, key := range validKeys {
		if err := srv.node.Wallet.Unlock(key); err == nil {
			return nil
		}
	}
	return modules.ErrBadEncryptionKey
}

// DumpStackTrace will dump the stack to a file in the stacktraces directory.
func (srv *Server) DumpStackTrace() error {
	// grab the stack
	buf := make([]byte, skymodules.StackSize)
	n := runtime.Stack(buf, true)

	tstr := time.Now().Format(time.RFC3339Nano)
	stackTracesDir := filepath.Join(srv.Dir, StackTracesFolderName)
	stackTraceFile := filepath.Join(stackTracesDir, "stack-trace-"+tstr+".txt")

	// ensure the directory exists
	err := os.MkdirAll(stackTracesDir, skymodules.DefaultDirPerm)
	if err != nil {
		return err
	}

	// create the file
	stackFile, err := os.Create(stackTraceFile)
	if err != nil {
		return err
	}

	// dump the stack
	_, err = stackFile.WriteString(string(buf[:n]))
	if err != nil {
		return err
	}

	// close the file
	return stackFile.Close()
}

// NewAsync creates a new API server from the provided skymodules. The API will
// require authentication using HTTP basic auth if the supplied password is not
// the empty string. Usernames are ignored for authentication. This type of
// authentication sends passwords in plaintext and should therefore only be
// used if the APIaddr is localhost.
func NewAsync(APIaddr string, requiredUserAgent string, requiredPassword string, nodeParams node.NodeParams, loadStartTime time.Time) (*Server, <-chan error) {
	c := make(chan error, 1)
	defer close(c)

	var errChan <-chan error
	var n *node.Node
	s, err := func() (*Server, error) {
		// Create the server listener.
		listener, err := net.Listen("tcp", APIaddr)
		if err != nil {
			return nil, err
		}

		// Load the config file.
		cfg, err := skymodules.NewConfig(filepath.Join(nodeParams.Dir, skymodules.ConfigName))
		if err != nil {
			return nil, errors.AddContext(err, "failed to load siad config")
		}

		// Create dir.
		err = os.MkdirAll(nodeParams.Dir, skymodules.DefaultDirPerm)
		if err != nil {
			return nil, errors.AddContext(err, "failed to create server root dir")
		}

		// Create logger.
		logFile, err := os.OpenFile(filepath.Join(nodeParams.Dir, logFileName), os.O_WRONLY|os.O_APPEND|os.O_CREATE, skymodules.DefaultFilePerm)
		if err != nil {
			return nil, errors.AddContext(err, "failed to open api log file")
		}
		logger := log.New(logFile, "", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile|log.LUTC)

		// Create the api for the server.
		api := api.New(cfg, logger, requiredUserAgent, requiredPassword, nil, nil, nil, nil, nil, nil, nil, nil, nil)

		// Server dependencies.
		serverDeps := nodeParams.ServerDeps
		if serverDeps == nil {
			serverDeps = modules.ProdDependencies
		}

		srv := &Server{
			api: api,
			apiServer: &http.Server{
				Handler: api,

				// set reasonable timeout windows for requests, to prevent the Sia API
				// server from leaking file descriptors due to slow, disappearing, or
				// unreliable API clients.

				// ReadTimeout defines the maximum amount of time allowed to fully read
				// the request body. This timeout is applied to every handler in the
				// server.
				ReadTimeout: time.Minute * 360,

				// ReadHeaderTimeout defines the amount of time allowed to fully read the
				// request headers.
				ReadHeaderTimeout: time.Minute * 2,

				// IdleTimeout defines the maximum duration a HTTP Keep-Alive connection
				// the API is kept open with no activity before closing.
				IdleTimeout: time.Minute * 5,
			},
			serveChan:         make(chan struct{}),
			listener:          listener,
			logFile:           logFile,
			requiredUserAgent: requiredUserAgent,
			Dir:               nodeParams.Dir,

			staticDeps: serverDeps,
		}

		// Set the shutdown method to allow the api to shutdown the server.
		api.Shutdown = func() error {
			fmt.Println("API shutdown triggered, quitting...")
			return srv.Close()
		}

		// Spin up a goroutine that serves the API and closes srv.serveChan when
		// finished.
		go func() {
			srv.serveErr = srv.serve()
			close(srv.serveChan)
		}()

		// Create the Sia node for the server after the server was started.
		n, errChan = node.New(nodeParams, loadStartTime)
		if err := modules.PeekErr(errChan); err != nil {
			if isAddrInUseErr(err) {
				return nil, fmt.Errorf("%v; are you running another instance of siad?", err.Error())
			}
			return nil, errors.AddContext(err, "server is unable to create the Sia node")
		}

		// Make sure that the server wasn't shut down while loading the
		// skymodules.
		select {
		case <-srv.serveChan:
			// Server was shut down. Close node and exit.
			return srv, n.Close()
		default:
		}

		// Server wasn't shut down. Add node and replace skymodules.
		srv.node = n
		api.SetModules(n.Accounting, n.ConsensusSet, n.Explorer, n.Gateway, n.Host, n.Miner, n.Renter, n.TransactionPool, n.Wallet)
		return srv, nil
	}()
	if err != nil {
		if n != nil {
			err = errors.Compose(err, n.Close())
		}
		c <- err
		return nil, c
	}
	return s, errChan
}

// New creates a new API server from the provided skymodules. The API will
// require authentication using HTTP basic auth if the supplied password is not
// the empty string. Usernames are ignored for authentication. This type of
// authentication sends passwords in plaintext and should therefore only be
// used if the APIaddr is localhost.
func New(APIaddr string, requiredUserAgent string, requiredPassword string, nodeParams node.NodeParams, loadStartTime time.Time) (*Server, error) {
	// Wait for the node to be done loading.
	srv, errChan := NewAsync(APIaddr, requiredUserAgent, requiredPassword, nodeParams, loadStartTime)
	if err := <-errChan; err != nil {
		// Error occurred during async load. Close all skymodules.
		if build.Release == "standard" {
			fmt.Println("ERROR:", err)
		}
		return nil, err
	}
	return srv, nil
}

// isAddrInUseErr checks if the error corresponds to syscall.EADDRINUSE
func isAddrInUseErr(err error) bool {
	if opErr, ok := err.(*net.OpError); ok {
		if syscallErr, ok := opErr.Err.(*os.SyscallError); ok {
			return syscallErr.Err == syscall.EADDRINUSE
		}
	}
	return false
}
