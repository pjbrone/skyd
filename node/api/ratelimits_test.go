package api

import (
	"sync"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/NebulousLabs/ratelimit"
)

// TestRatelimitedResponseWriter is a unit test for the
// ratelimitedResponseWriter type.
func TestRatelimitedResponseWriter(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	w := newTestHTTPWriter()

	// Write to it. This should happen very quickly. in under 10ms.
	start := time.Now()
	_, err := w.Write(fastrand.Bytes(1000))
	d := time.Since(start)
	if err != nil {
		t.Fatal(err)
	}
	if d > 10*time.Millisecond {
		t.Fatal("took too long")
	}

	// Limit it to take 1 second for the same amount of data.
	// To do so we set it to a 500 bytes per second and a 500 byte packet
	// size while writing 1000 bytes. The first packet is written
	// immediately but the second one takes a second to write.
	rl := ratelimit.NewRateLimit(0, 500, 500)
	rw := newRatelimitedResponseWriter(w, rl)

	_, err = rw.Write(fastrand.Bytes(1000))
	d = time.Since(start)
	if err != nil {
		t.Fatal(err)
	}
	if d < time.Second {
		t.Fatal("wrong duration", d)
	}
}

// TestRatelimits is a unit test for the ratelimits type. It makes sure that
// ratelimiting multiple connections with the same uid causes the bandwidth to
// be shared and it checks that ratelimits are cleaned up correctly in memory.
func TestRatelimits(t *testing.T) {
	rls := newRatelimits()
	uid := "foo"

	w := newTestHTTPWriter()

	rw1, done1 := rls.LimitDownload(w, uid, RatelimitPacketSize)
	if rls.activeDownloads[uid].refcount != 1 {
		t.Fatal("wrong refcount")
	}
	rw2, done2 := rls.LimitDownload(w, uid, RatelimitPacketSize)
	if rls.activeDownloads[uid].refcount != 2 {
		t.Fatal("wrong refcount")
	}

	_, err := rw1.Write(fastrand.Bytes(RatelimitPacketSize))
	if err != nil {
		t.Fatal()
	}

	var d1, d2 time.Duration
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		start := time.Now()
		defer wg.Done()
		_, err := rw1.Write(fastrand.Bytes(RatelimitPacketSize))
		if err != nil {
			t.Error()
		}
		d1 = time.Since(start)
	}()
	go func() {
		start := time.Now()
		defer wg.Done()
		_, err := rw2.Write(fastrand.Bytes(RatelimitPacketSize))
		if err != nil {
			t.Error()
		}
		d2 = time.Since(start)
	}()
	wg.Wait()

	d := d1 + d2
	if d < 2*time.Second {
		t.Fatal("wrong duration", d)
	}

	done1()
	if rls.activeDownloads[uid].refcount != 1 {
		t.Fatal("wrong refcount")
	}

	done2()
	if _, exists := rls.activeDownloads[uid]; exists {
		t.Fatal("shouldn't exist")
	}
}
