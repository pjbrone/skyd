module gitlab.com/SkynetLabs/skyd

go 1.18

require (
	github.com/aead/chacha20 v0.0.0-20180709150244-8b13a72661da
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/dchest/threefish v0.0.0-20120919164726-3ecf4c494abf
	github.com/eventials/go-tus v0.0.0-20200718001131-45c7ec8f5d59
	github.com/gorilla/websocket v1.5.0
	github.com/hanwen/go-fuse/v2 v2.1.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/klauspost/reedsolomon v1.10.0
	github.com/montanaflynn/stats v0.6.3
	github.com/opentracing/opentracing-go v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/square/mongo-lock v0.0.0-20201208161834-4db518ed7fb2
	github.com/tus/tusd v1.9.0
	github.com/uber/jaeger-client-go v2.27.0+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	github.com/vbauerster/mpb/v5 v5.0.3
	gitlab.com/NebulousLabs/encoding v0.0.0-20200604091946-456c3dc907fe
	gitlab.com/NebulousLabs/entropy-mnemonics v0.0.0-20181018051301-7532f67e3500
	gitlab.com/NebulousLabs/errors v0.0.0-20200929122200-06c536cf6975
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/log v0.0.0-20210609172545-77f6775350e2
	gitlab.com/NebulousLabs/ratelimit v0.0.0-20200811080431-99b8f0768b2e
	gitlab.com/NebulousLabs/siamux v0.0.2-0.20220819160410-b3fb3772a220
	gitlab.com/NebulousLabs/threadgroup v0.0.0-20200608151952-38921fbef213
	gitlab.com/NebulousLabs/writeaheadlog v0.0.0-20200618142844-c59a90f49130
	go.mongodb.org/mongo-driver v1.4.2
	go.sia.tech/core v0.0.0-20220524010238-790a68db5817
	go.sia.tech/siad v1.5.9
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
	golang.org/x/net v0.0.0-20220809184613-07c6da5e1ced
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1 // indirect
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/VividCortex/ewma v1.1.1 // indirect
	github.com/acarl005/stripansi v0.0.0-20180116102854-5a71ef0e047d // indirect
	github.com/aws/aws-sdk-go v1.43.31 // indirect
	github.com/bmizerany/pat v0.0.0-20170815010413-6226ea591a40 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/hdevalence/ed25519consensus v0.0.0-20220222234857-c00d1f31bab3 // indirect
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/klauspost/cpuid/v2 v2.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/NebulousLabs/bolt v1.4.4 // indirect
	gitlab.com/NebulousLabs/demotemutex v0.0.0-20151003192217-235395f71c40 // indirect
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20211002182029-11da932010b6 // indirect
	gitlab.com/NebulousLabs/merkletree v0.0.0-20200118113624-07fbf710afc4 // indirect
	gitlab.com/NebulousLabs/monitor v0.0.0-20191205095550-2b0fd3e1012a // indirect
	gitlab.com/NebulousLabs/persist v0.0.0-20200605115618-007e5e23d877 // indirect
	go.sia.tech/mux v1.0.1 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220808155132-1c4a2a72c664 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	lukechampine.com/frand v1.4.2 // indirect
)
