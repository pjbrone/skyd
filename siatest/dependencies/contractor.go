package dependencies

// NewContractForceNotGoodForUpload is a dependency injection for forcing
// contracts to appear as not good for upload.
func NewContractForceNotGoodForUpload() *DependencyWithDisableAndEnable {
	return newDependencywithDisableAndEnable("ContractForceNotGoodForUpload")
}
