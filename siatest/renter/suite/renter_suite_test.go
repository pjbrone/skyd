package suite

import (
	"encoding/json"
	"fmt"
	"math"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strconv"
	"sync"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"

	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/node"
	"gitlab.com/SkynetLabs/skyd/node/api"
	"gitlab.com/SkynetLabs/skyd/siatest"
	"gitlab.com/SkynetLabs/skyd/siatest/dependencies"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"gitlab.com/SkynetLabs/skyd/skymodules/renter"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/persist"
	"go.sia.tech/siad/types"
)

// TestRenterOne executes a number of subtests using the same TestGroup to save
// time on initialization
func TestRenterOne(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create a group for the subtests
	groupParams := siatest.GroupParams{
		Hosts:   5,
		Renters: 1,
		Miners:  1,
	}
	groupDir := renterTestDir(t.Name())

	// Specify subtests to run
	subTests := []siatest.SubTest{
		{Name: "TestDownloadMultipleLargeSectors", Test: testDownloadMultipleLargeSectors},
		{Name: "TestLocalRepair", Test: testLocalRepair},
		{Name: "TestClearDownloadHistory", Test: testClearDownloadHistory},
		{Name: "TestDownloadAfterRenew", Test: testDownloadAfterRenew},
		{Name: "TestDirectories", Test: testDirectories},
		{Name: "TestAlertsSorted", Test: testAlertsSorted},
		{Name: "TestPriceTablesUpdated", Test: testPriceTablesUpdated},
	}

	// Run tests
	if err := siatest.RunSubTests(t, groupParams, groupDir, subTests); err != nil {
		t.Fatal(err)
	}
}

// TestRenterTwo executes a number of subtests using the same TestGroup to
// save time on initialization
func TestRenterTwo(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create a group for the subtests
	groupParams := siatest.GroupParams{
		Hosts:   5,
		Renters: 1,
		Miners:  1,
	}
	groupDir := renterTestDir(t.Name())

	// Specify subtests to run
	subTests := []siatest.SubTest{
		{Name: "TestReceivedFieldEqualsFileSize", Test: testReceivedFieldEqualsFileSize},
		{Name: "TestSingleFileGet", Test: testSingleFileGet},
		{Name: "TestSiaFileTimestamps", Test: testSiafileTimestamps},
		{Name: "TestZeroByteFile", Test: testZeroByteFile},
		{Name: "TestUploadWithAndWithoutForceParameter", Test: testUploadWithAndWithoutForceParameter},
	}

	// Run tests
	if err := siatest.RunSubTests(t, groupParams, groupDir, subTests); err != nil {
		t.Fatal(err)
	}
}

// TestRenterThree executes a number of subtests using the same TestGroup to
// save time on initialization
func TestRenterThree(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create a group for the subtests
	groupParams := siatest.GroupParams{
		Hosts:   5,
		Renters: 1,
		Miners:  1,
	}
	groupDir := renterTestDir(t.Name())

	// Specify subtests to run
	subTests := []siatest.SubTest{
		{Name: "TestAllowanceDefaultSet", Test: testAllowanceDefaultSet},
		{Name: "TestFileAvailableAndRecoverable", Test: testFileAvailableAndRecoverable},
		{Name: "TestSetFileStuck", Test: testSetFileStuck},
		{Name: "TestCancelAsyncDownload", Test: testCancelAsyncDownload},
		{Name: "TestUploadDownload", Test: testUploadDownload}, // Needs to be last as it impacts hosts
	}

	// Run tests
	if err := siatest.RunSubTests(t, groupParams, groupDir, subTests); err != nil {
		t.Fatal(err)
	}
}

// TestRenterFour executes a number of subtests using the same TestGroup to
// save time on initialization
func TestRenterFour(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Create a group for the subtests
	groupParams := siatest.GroupParams{
		Hosts:   5,
		Renters: 1,
		Miners:  1,
	}
	groupDir := renterTestDir(t.Name())

	// Specify subtests to run
	subTests := []siatest.SubTest{
		{Name: "TestValidateSiaPath", Test: testValidateSiaPath},
		{Name: "TestNextPeriod", Test: testNextPeriod},
		{Name: "TestPauseAndResumeRepairAndUploads", Test: testPauseAndResumeRepairAndUploads},
		{Name: "TestDownloadServedFromDisk", Test: testDownloadServedFromDisk},
		{Name: "TestEscapeSiaPath", Test: testEscapeSiaPath}, // Runs last because it uploads many files
	}

	// Run tests
	if err := siatest.RunSubTests(t, groupParams, groupDir, subTests); err != nil {
		t.Fatal(err)
	}
}

// testSiafileTimestamps tests if timestamps are set correctly when creating,
// uploading, downloading and modifying a file.
func testSiafileTimestamps(t *testing.T, tg *siatest.TestGroup) {
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}
	// Grab the renter.
	r := tg.Renters()[0]

	// Get the current time.
	beforeUploadTime := time.Now()

	// Upload a new file.
	_, rf, err := r.UploadNewFileBlocking(100+siatest.Fuzz(), 1, 1, false)
	if err != nil {
		t.Fatal(err)
	}

	// Get the time again.
	afterUploadTime := time.Now()

	// Get the timestamps using the API.
	fi, err := r.File(rf)
	if err != nil {
		t.Fatal(err)
	}

	// The timestamps should all be between beforeUploadTime and
	// afterUploadTime.
	if fi.CreateTime.Before(beforeUploadTime) || fi.CreateTime.After(afterUploadTime) {
		t.Fatal("CreateTime was not within the correct interval")
	}
	if fi.AccessTime.Before(beforeUploadTime) || fi.AccessTime.After(afterUploadTime) {
		t.Fatal("AccessTime was not within the correct interval")
	}
	if fi.ChangeTime.Before(beforeUploadTime) || fi.ChangeTime.After(afterUploadTime) {
		t.Fatal("ChangeTime was not within the correct interval")
	}
	if fi.ModificationTime.Before(beforeUploadTime) || fi.ModificationTime.After(afterUploadTime) {
		t.Fatal("ModificationTime was not within the correct interval")
	}

	// After uploading a file the AccessTime, ChangeTime and ModificationTime should be
	// the same.
	if fi.AccessTime != fi.ChangeTime || fi.ChangeTime != fi.ModificationTime {
		t.Fatal("AccessTime, ChangeTime and ModificationTime are not the same")
	}

	// The CreateTime should precede the other timestamps.
	if fi.CreateTime.After(fi.AccessTime) {
		t.Fatal("CreateTime should before other timestamps")
	}

	// Get the time before starting the download.
	beforeDownloadTime := time.Now()

	// Download the file.
	_, _, err = r.DownloadByStream(rf)
	if err != nil {
		t.Fatal(err)
	}

	// Get the time after the download is done.
	afterDownloadTime := time.Now()

	// Get the timestamps using the API.
	fi2, err := r.File(rf)
	if err != nil {
		t.Fatal(err)
	}

	// Only the AccessTime should have changed.
	if fi2.AccessTime.Before(beforeDownloadTime) || fi2.AccessTime.After(afterDownloadTime) {
		t.Fatal("AccessTime was not within the correct interval")
	}
	if fi.CreateTime != fi2.CreateTime {
		t.Fatal("CreateTime changed after download")
	}
	if fi.ChangeTime != fi2.ChangeTime {
		t.Fatal("ChangeTime changed after download")
	}
	if fi.ModificationTime != fi2.ModificationTime {
		t.Fatal("ModificationTime changed after download")
	}

	// TODO Once we can change the localPath using the API, check that it only
	// changes the ChangeTime to do so.

	// Get the time before renaming.
	beforeRenameTime := time.Now()

	newSiaPath, err := skymodules.NewSiaPath("newsiapath")
	if err != nil {
		t.Fatal(err)
	}
	// Rename the file and check that only the ChangeTime changed.
	rf, err = r.Rename(rf, newSiaPath)
	if err != nil {
		t.Fatal(err)
	}

	// Get the time after renaming.
	afterRenameTime := time.Now()

	// Get the timestamps using the API.
	fi3, err := r.File(rf)
	if err != nil {
		t.Fatal(err)
	}

	// Only the ChangeTime should have changed.
	if fi3.ChangeTime.Before(beforeRenameTime) || fi3.ChangeTime.After(afterRenameTime) {
		t.Fatal("ChangeTime was not within the correct interval")
	}
	if fi2.CreateTime != fi3.CreateTime {
		t.Fatal("CreateTime changed after download")
	}
	if fi2.AccessTime != fi3.AccessTime {
		t.Fatal("AccessTime changed after download")
	}
	if fi2.ModificationTime != fi3.ModificationTime {
		t.Fatal("ModificationTime changed after download")
	}
}

// testAllowanceDefaultSet tests that a renter's allowance is correctly set to
// the defaults after creating it and therefore confirming that the API
// endpoint and siatest package both work.
func testAllowanceDefaultSet(t *testing.T, tg *siatest.TestGroup) {
	if len(tg.Renters()) == 0 {
		t.Fatal("Test requires at least 1 renter")
	}
	// Get allowance.
	r := tg.Renters()[0]
	rg, err := r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	// Make sure that the allowance was set correctly.
	if !reflect.DeepEqual(rg.Settings.Allowance, siatest.DefaultAllowance) {
		expected, _ := json.Marshal(siatest.DefaultAllowance)
		was, _ := json.Marshal(rg.Settings.Allowance)
		t.Log("Expected", string(expected))
		t.Log("Was", string(was))
		t.Fatal("Renter's allowance doesn't match siatest.DefaultAllowance")
	}
}

// testReceivedFieldEqualsFileSize tests that the bug that caused finished
// downloads to stall in the UI and siac is gone.
func testReceivedFieldEqualsFileSize(t *testing.T, tg *siatest.TestGroup) {
	// Make sure the test has enough hosts.
	if len(tg.Hosts()) < 4 {
		t.Fatal("testReceivedFieldEqualsFileSize requires at least 4 hosts")
	}
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	// Clear the download history to make sure it's empty before we start the test.
	err := r.RenterClearAllDownloadsPost()
	if err != nil {
		t.Fatal(err)
	}

	// Upload a file.
	dataPieces := uint64(3)
	parityPieces := uint64(1)
	fileSize := int(modules.SectorSize)
	lf, rf, err := r.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}

	// This code sums up the 'received' variable in a similar way the renter
	// does it. We use it to find a fetchLen for which received != fetchLen due
	// to the implicit rounding of the unsigned integers.
	var fetchLen uint64
	for fetchLen = uint64(100); ; fetchLen++ {
		received := uint64(0)
		for piecesCompleted := uint64(1); piecesCompleted <= dataPieces; piecesCompleted++ {
			received += fetchLen / dataPieces
		}
		if received != fetchLen {
			break
		}
	}

	// Download fetchLen bytes of the file.
	_, _, err = r.DownloadToDiskPartial(rf, lf, false, 0, fetchLen)
	if err != nil {
		t.Fatal(err)
	}

	// Get the download.
	rdg, err := r.RenterDownloadsGet()
	if err != nil {
		t.Fatal(err)
	}
	d := rdg.Downloads[0]

	// Make sure that 'Received' matches the amount of data we fetched.
	if !d.Completed {
		t.Error("Download should be completed but wasn't")
	}
	if d.Received != fetchLen {
		t.Errorf("Received was %v but should be %v", d.Received, fetchLen)
	}
	// Compare siapaths.
	rdgr, err := r.RenterDownloadsRootGet()
	if err != nil {
		t.Fatal(err)
	}
	if !d.SiaPath.Equals(rf.SiaPath()) {
		t.Fatal(d.SiaPath.String(), rf.SiaPath().String())
	}
	sp, err := rf.SiaPath().Rebase(skymodules.RootSiaPath(), skymodules.UserFolder)
	if err != nil {
		t.Fatal(err)
	}
	if !rdgr.Downloads[0].SiaPath.Equals(sp) {
		t.Fatal(d.SiaPath.String(), rf.SiaPath().String())
	}
}

// testClearDownloadHistory makes sure that the download history is
// properly cleared when called through the API
func testClearDownloadHistory(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	rdg, err := r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	numDownloads := 10
	if len(rdg.Downloads) < numDownloads {
		remainingDownloads := numDownloads - len(rdg.Downloads)
		rf, err := r.RenterFilesGet(false)
		if err != nil {
			t.Fatal(err)
		}
		// Check if the renter has any files
		// Upload a file if none
		if len(rf.Files) == 0 {
			dataPieces := uint64(1)
			parityPieces := uint64(1)
			fileSize := 100 + siatest.Fuzz()
			_, _, err := r.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
			if err != nil {
				t.Fatal("Failed to upload a file for testing: ", err)
			}
			rf, err = r.RenterFilesGet(false)
			if err != nil {
				t.Fatal(err)
			}
		}
		// Download files to build download history
		dest := filepath.Join(siatest.SiaTestingDir, strconv.Itoa(fastrand.Intn(math.MaxInt32)))
		for i := 0; i < remainingDownloads; i++ {
			_, err = r.RenterDownloadGet(rf.Files[0].SiaPath, dest, 0, rf.Files[0].Filesize, false, false, false)
			if err != nil {
				t.Fatal("Could not Download file:", err)
			}
		}
		rdg, err = r.RenterDownloadsGet()
		if err != nil {
			t.Fatal("Could not get download history:", err)
		}
		// Confirm download history is not empty
		if len(rdg.Downloads) != numDownloads {
			t.Fatalf("Not all downloads added to download history: only %v downloads added, expected %v", len(rdg.Downloads), numDownloads)
		}
	}
	numDownloads = len(rdg.Downloads)

	// Check removing one download from history
	// Remove First Download
	timestamp := rdg.Downloads[0].StartTime
	err = r.RenterClearDownloadsRangePost(timestamp, timestamp)
	if err != nil {
		t.Fatal("Error in API endpoint to remove download from history:", err)
	}
	numDownloads--
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	if len(rdg.Downloads) != numDownloads {
		t.Fatalf("Download history not reduced: history has %v downloads, expected %v", len(rdg.Downloads), numDownloads)
	}
	i := sort.Search(len(rdg.Downloads), func(i int) bool { return rdg.Downloads[i].StartTime.Equal(timestamp) })
	if i < len(rdg.Downloads) {
		t.Fatal("Specified download not removed from history")
	}
	// Remove Last Download
	timestamp = rdg.Downloads[len(rdg.Downloads)-1].StartTime
	err = r.RenterClearDownloadsRangePost(timestamp, timestamp)
	if err != nil {
		t.Fatal("Error in API endpoint to remove download from history:", err)
	}
	numDownloads--
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	if len(rdg.Downloads) != numDownloads {
		t.Fatalf("Download history not reduced: history has %v downloads, expected %v", len(rdg.Downloads), numDownloads)
	}
	i = sort.Search(len(rdg.Downloads), func(i int) bool { return rdg.Downloads[i].StartTime.Equal(timestamp) })
	if i < len(rdg.Downloads) {
		t.Fatal("Specified download not removed from history")
	}

	// Check Clear Before
	timestamp = rdg.Downloads[len(rdg.Downloads)-2].StartTime
	err = r.RenterClearDownloadsBeforePost(timestamp)
	if err != nil {
		t.Fatal("Error in API endpoint to clear download history before timestamp:", err)
	}
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	i = sort.Search(len(rdg.Downloads), func(i int) bool { return rdg.Downloads[i].StartTime.Before(timestamp) })
	if i < len(rdg.Downloads) {
		t.Fatal("Download found that was before given time")
	}

	// Check Clear After
	timestamp = rdg.Downloads[1].StartTime
	err = r.RenterClearDownloadsAfterPost(timestamp)
	if err != nil {
		t.Fatal("Error in API endpoint to clear download history after timestamp:", err)
	}
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	i = sort.Search(len(rdg.Downloads), func(i int) bool { return rdg.Downloads[i].StartTime.After(timestamp) })
	if i < len(rdg.Downloads) {
		t.Fatal("Download found that was after given time")
	}

	// Check clear range
	before := rdg.Downloads[1].StartTime
	after := rdg.Downloads[len(rdg.Downloads)-1].StartTime
	err = r.RenterClearDownloadsRangePost(after, before)
	if err != nil {
		t.Fatal("Error in API endpoint to remove range of downloads from history:", err)
	}
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	i = sort.Search(len(rdg.Downloads), func(i int) bool {
		return rdg.Downloads[i].StartTime.Before(before) && rdg.Downloads[i].StartTime.After(after)
	})
	if i < len(rdg.Downloads) {
		t.Fatal("Not all downloads from range removed from history")
	}

	// Check clearing download history
	err = r.RenterClearAllDownloadsPost()
	if err != nil {
		t.Fatal("Error in API endpoint to clear download history:", err)
	}
	rdg, err = r.RenterDownloadsGet()
	if err != nil {
		t.Fatal("Could not get download history:", err)
	}
	if len(rdg.Downloads) != 0 {
		t.Fatalf("Download history not cleared: history has %v downloads, expected 0", len(rdg.Downloads))
	}
}

// testDirectories checks the functionality of directories in the Renter
func testDirectories(t *testing.T, tg *siatest.TestGroup) {
	// Grab Renter
	r := tg.Renters()[0]

	// Test Directory endpoint for creating empty directory
	rd, err := r.UploadNewDirectory()
	if err != nil {
		t.Fatal(err)
	}

	// Check directory
	rgd, err := r.RenterDirGet(rd.SiaPath())
	if err != nil {
		t.Fatal(err)
	}
	// Directory should return 0 FileInfos and 1 DirectoryInfo with would belong
	// to the directory itself
	if len(rgd.Directories) != 1 {
		t.Fatal("Expected 1 DirectoryInfo to be returned but got:", len(rgd.Directories))
	}
	if rgd.Directories[0].SiaPath != rd.SiaPath() {
		t.Fatalf("SiaPaths do not match %v and %v", rgd.Directories[0].SiaPath, rd.SiaPath())
	}
	if len(rgd.Files) != 0 {
		t.Fatal("Expected no files in directory but found:", len(rgd.Files))
	}

	// Check uploading file to new subdirectory
	// Create local file
	size := 100 + siatest.Fuzz()
	fd := r.FilesDir()
	ld, err := fd.CreateDir("subDir1/subDir2/subDir3-" + persist.RandomSuffix())
	if err != nil {
		t.Fatal(err)
	}
	lf, err := ld.NewFile(size)
	if err != nil {
		t.Fatal(err)
	}

	// Upload file
	dataPieces := uint64(1)
	parityPieces := uint64(1)
	rf, err := r.UploadBlocking(lf, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Check directory that file was uploaded to
	siaPath, err := rf.SiaPath().Dir()
	if err != nil {
		t.Fatal(err)
	}
	rgd, err = r.RenterDirGet(siaPath)
	if err != nil {
		t.Fatal(err)
	}
	// Directory should have 1 file and 0 sub directories
	if len(rgd.Directories) != 1 {
		t.Fatal("Expected 1 DirectoryInfo to be returned but got:", len(rgd.Directories))
	}
	if len(rgd.Files) != 1 {
		t.Fatal("Expected 1 file in directory but found:", len(rgd.Files))
	}

	// Check parent directory
	siaPath, err = siaPath.Dir()
	if err != nil {
		t.Fatal(err)
	}
	rgd, err = r.RenterDirGet(siaPath)
	if err != nil {
		t.Fatal(err)
	}
	// Directory should have 0 files and 1 sub directory
	if len(rgd.Directories) != 2 {
		t.Fatal("Expected 2 DirectoryInfos to be returned but got:", len(rgd.Directories))
	}
	if len(rgd.Files) != 0 {
		t.Fatal("Expected 0 files in directory but found:", len(rgd.Files))
	}

	// Test renaming subdirectory
	subDir1, err := skymodules.NewSiaPath("subDir1")
	if err != nil {
		t.Fatal(err)
	}
	newSiaPath := skymodules.RandomSiaPath()
	if err = r.RenterDirRenamePost(subDir1, newSiaPath); err != nil {
		t.Fatal(err)
	}
	// Renamed directory should have 0 files and 1 sub directory.
	rgd, err = r.RenterDirGet(newSiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if len(rgd.Files) != 0 {
		t.Fatalf("Renamed dir should have 0 files but had %v", len(rgd.Files))
	}
	if len(rgd.Directories) != 2 {
		t.Fatalf("Renamed dir should have 1 sub directory but had %v",
			len(rgd.Directories)-1)
	}
	// Subdir of renamed dir should have 0 files and 1 sub directory.
	rgd, err = r.RenterDirGet(rgd.Directories[1].SiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if len(rgd.Files) != 0 {
		t.Fatalf("Renamed dir should have 0 files but had %v", len(rgd.Files))
	}
	if len(rgd.Directories) != 2 {
		t.Fatalf("Renamed dir should have 1 sub directory but had %v",
			len(rgd.Directories)-1)
	}
	// SubSubdir of renamed dir should have 1 file and 0 sub directories.
	rgd, err = r.RenterDirGet(rgd.Directories[1].SiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if len(rgd.Files) != 1 {
		t.Fatalf("Renamed dir should have 1 file but had %v", len(rgd.Files))
	}
	if len(rgd.Directories) != 1 {
		t.Fatalf("Renamed dir should have 0 sub directories but had %v",
			len(rgd.Directories)-1)
	}
	// Try downloading the renamed file.
	if _, _, err := r.RenterDownloadHTTPResponseGet(rgd.Files[0].SiaPath, 0, uint64(size), true, false); err != nil {
		t.Fatal(err)
	}

	// Check that the old siadir was deleted from disk
	_, err = os.Stat(subDir1.SiaDirSysPath(r.RenterFilesDir()))
	if !os.IsNotExist(err) {
		t.Fatal("Expected IsNotExist err, but got err:", err)
	}

	// create a file to test file deletion
	lf1, err := ld.NewFile(size)
	if err != nil {
		t.Fatal(err)
	}
	rf1, err := r.UploadBlocking(lf1, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Test deleting a file by its relative path
	err = r.RenterFileDeletePost(rf1.SiaPath())
	if err != nil {
		t.Fatal(err)
	}

	// Test deleting directory
	if err = r.RenterDirDeletePost(rd.SiaPath()); err != nil {
		t.Fatal(err)
	}

	// Create a new set of remote files and dirs, so we can test deleting with a
	// root path
	rd2, err := r.UploadNewDirectory()
	if err != nil {
		t.Fatal(err)
	}
	ld2, err := fd.CreateDir("subDir1a/subDir2a/subDir3a-" + persist.RandomSuffix())
	if err != nil {
		t.Fatal(err)
	}
	lf2, err := ld2.NewFile(size)
	if err != nil {
		t.Fatal(err)
	}
	rf2, err := r.UploadBlocking(lf2, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Test deleting a file by its root path
	rf2RootPath, err := skymodules.NewSiaPath("/home/user/" + rf2.SiaPath().Path)
	err = r.RenterFileDeleteRootPost(rf2RootPath)
	if err != nil {
		t.Fatal(fmt.Errorf(err.Error() + " => " + rf2RootPath.Path))
	}

	// Test deleting directory by its root path
	rd2RootPath, err := skymodules.NewSiaPath("/home/user/" + rd2.SiaPath().Path)
	if err = r.RenterDirDeleteRootPost(rd2RootPath); err != nil {
		t.Fatal(fmt.Errorf(err.Error() + " => " + rd2RootPath.Path))
	}

	// Check that siadir was deleted from disk
	_, err = os.Stat(rd.SiaPath().SiaDirSysPath(r.RenterFilesDir()))
	if !os.IsNotExist(err) {
		t.Fatal("Expected IsNotExist err, but got err:", err)
	}
	// Check that siadir was deleted from disk by root path
	_, err = os.Stat(rd2.SiaPath().SiaDirSysPath(r.RenterFilesDir()))
	if !os.IsNotExist(err) {
		t.Fatal("Expected IsNotExist err, but got err:", err)
	}
}

// testAlertsSorted checks that the alerts returned by the /daemon/alerts
// endpoint are sorted by severity.
func testAlertsSorted(t *testing.T, tg *siatest.TestGroup) {
	// Grab Renter
	r := tg.Renters()[0]
	dag, err := r.DaemonAlertsGet()
	if err != nil {
		t.Fatal(err)
	}
	if len(dag.Alerts) < 3 {
		t.Fatalf("renter should have at least %v alerts registered but was %v", 3, len(dag.Alerts))
	}
	sorted := sort.SliceIsSorted(dag.Alerts, func(i, j int) bool {
		return dag.Alerts[i].Severity > dag.Alerts[j].Severity
	})
	if !sorted {
		t.Log("alerts:", dag.Alerts)
		t.Fatal("alerts are not sorted by severity")
	}
}

// testDownloadAfterRenew makes sure that we can still download a file
// after the contract period has ended.
func testDownloadAfterRenew(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	renter := tg.Renters()[0]
	// Upload file, creating a piece for each host in the group
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	fileSize := 100 + siatest.Fuzz()
	_, remoteFile, err := renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	// Mine enough blocks for the next period to start. This means the
	// contracts should be renewed and the data should still be available for
	// download.
	miner := tg.Miners()[0]
	for i := types.BlockHeight(0); i < siatest.DefaultAllowance.Period; i++ {
		if err := miner.MineBlock(); err != nil {
			t.Fatal(err)
		}
	}
	// Download the file synchronously directly into memory.
	_, _, err = renter.DownloadByStream(remoteFile)
	if err != nil {
		t.Fatal(err)
	}
}

// testDownloadMultipleLargeSectors downloads multiple large files (>5 Sectors)
// in parallel and makes sure that the downloads are blocking each other.
func testDownloadMultipleLargeSectors(t *testing.T, tg *siatest.TestGroup) {
	// parallelDownloads is the number of downloads that are run in parallel.
	parallelDownloads := 10
	// fileSize is the size of the downloaded file.
	fileSize := siatest.Fuzz()
	if build.VLONG {
		fileSize += int(50 * modules.SectorSize)
	} else {
		fileSize += int(10 * modules.SectorSize)
	}
	// set download limits and reset them after test.
	// uniqueRemoteFiles is the number of files that will be uploaded to the
	// network. Downloads will choose the remote file to download randomly.
	uniqueRemoteFiles := 5
	// Create a custom renter with a dependency and remove it again after the test
	// is done.
	renterParams := node.Renter(filepath.Join(renterTestDir(t.Name()), "renter"))
	renterParams.RenterDeps = &dependencies.DependencyPostponeWritePiecesRecovery{}
	nodes, err := tg.AddNodes(renterParams)
	if err != nil {
		t.Fatal(err)
	}
	renter := nodes[0]
	defer func() {
		if err := tg.RemoveNode(renter); err != nil {
			t.Fatal(err)
		}
	}()

	// Upload files
	dataPieces := uint64(len(tg.Hosts())) - 1
	parityPieces := uint64(1)
	remoteFiles := make([]*siatest.RemoteFile, 0, uniqueRemoteFiles)
	for i := 0; i < uniqueRemoteFiles; i++ {
		_, remoteFile, err := renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
		if err != nil {
			t.Fatal("Failed to upload a file for testing: ", err)
		}
		remoteFiles = append(remoteFiles, remoteFile)
	}

	// set download limits and reset them after test.
	if err := renter.RenterRateLimitPost(int64(fileSize)*2, 0); err != nil {
		t.Fatal("failed to set renter bandwidth limit", err)
	}
	defer func() {
		if err := renter.RenterRateLimitPost(0, 0); err != nil {
			t.Error("failed to reset renter bandwidth limit", err)
		}
	}()

	// Randomly download using download to file and download to stream methods.
	wg := new(sync.WaitGroup)
	for i := 0; i < parallelDownloads; i++ {
		wg.Add(1)
		go func() {
			var err error
			var rf = remoteFiles[fastrand.Intn(len(remoteFiles))]
			if fastrand.Intn(2) == 0 {
				_, _, err = renter.DownloadByStream(rf)
			} else {
				_, _, err = renter.DownloadToDisk(rf, false)
			}
			if err != nil {
				t.Error("Download failed:", err)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

// testLocalRepair tests if a renter correctly repairs a file from disk
// after a host goes offline.
func testLocalRepair(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	renterNode := tg.Renters()[0]

	// Check that we have enough hosts for this test.
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}

	// Set fileSize and redundancy for upload
	fileSize := int(modules.SectorSize)
	dataPieces := uint64(2)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces

	// Upload file
	_, remoteFile, err := renterNode.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Take down hosts until enough are missing that the chunks get marked as
	// stuck after repairs.
	var hostsRemoved uint64
	for hostsRemoved = 0; float64(hostsRemoved)/float64(parityPieces) < renter.AlertSiafileLowRedundancyThreshold; hostsRemoved++ {
		if err := tg.RemoveNode(tg.Hosts()[0]); err != nil {
			t.Fatal("Failed to shutdown host", err)
		}
	}
	expectedRedundancy := float64(dataPieces+parityPieces-hostsRemoved) / float64(dataPieces)
	if err := renterNode.WaitForDecreasingRedundancy(remoteFile, expectedRedundancy); err != nil {
		t.Fatal("Redundancy isn't decreasing", err)
	}
	// We should still be able to download
	if _, _, err := renterNode.DownloadByStream(remoteFile); err != nil {
		t.Fatal("Failed to download file", err)
	}
	// Check that the alert for low redundancy was set.
	err = build.Retry(100, 100*time.Millisecond, func() error {
		dag, err := renterNode.DaemonAlertsGet()
		if err != nil {
			return errors.AddContext(err, "Failed to get alerts")
		}
		f, err := renterNode.File(remoteFile)
		if err != nil {
			return err
		}
		var found bool
		for _, alert := range dag.Alerts {
			expectedCause := fmt.Sprintf("Siafile 'home/user/%v' has a health of %v and redundancy of %v", remoteFile.SiaPath().String(), f.MaxHealth, f.Redundancy)
			if alert.Msg == renter.AlertMSGSiafileLowRedundancy &&
				alert.Cause == expectedCause {
				found = true
			}
		}
		if !found {
			return fmt.Errorf("Correct alert wasn't registered (#alerts: %v)", len(dag.Alerts))
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// Bring up hosts to replace the ones that went offline.
	_, err = tg.AddNodeN(node.HostTemplate, int(hostsRemoved))
	if err != nil {
		t.Fatal("Failed to create a new host", err)
	}
	if err := renterNode.WaitForUploadHealth(remoteFile); err != nil {
		t.Fatal("File wasn't repaired", err)
	}
	// Make sure that the file system is updated
	err = renterNode.RenterBubblePost(skymodules.RootSiaPath(), true)
	if err != nil {
		t.Fatal(err)
	}
	// File should not report any stuck chunks
	err = build.Retry(100, 100*time.Millisecond, func() error {
		fi, err := renterNode.File(remoteFile)
		if err != nil {
			return err
		}
		if fi.Stuck || fi.NumStuckChunks != 0 {
			return fmt.Errorf("File is still stuck %v with %v stuck chunks", fi.Stuck, fi.NumStuckChunks)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// We should be able to download
	if _, _, err := renterNode.DownloadByStream(remoteFile); err != nil {
		t.Fatal("Failed to download file", err)
	}
}

// testPriceTablesUpdated verfies the workers' price tables are updated and stay
// recent with the host
func testPriceTablesUpdated(t *testing.T, tg *siatest.TestGroup) {
	r := tg.Renters()[0]

	// Get the worker status
	rwg, err := r.RenterWorkersGet()
	if err != nil {
		t.Fatal(err)
	}

	// Get a random worker
	var host types.SiaPublicKey
	for _, worker := range rwg.Workers {
		host = worker.HostPubKey
		break
	}

	// Wait until that worker has been able to update its price table, when that
	// is the case we save its current update and expiry time.
	var ut, et time.Time
	err = build.Retry(100, 100*time.Millisecond, func() error {
		rwg, err := r.RenterWorkersGet()
		if err != nil {
			return err
		}

		var ws *skymodules.WorkerStatus
		for i := range rwg.Workers {
			worker := rwg.Workers[i]
			if worker.HostPubKey.Equals(host) {
				ws = &worker
				break
			}
		}
		if ws == nil {
			return errors.New("worker not found")
		}

		if !ws.PriceTableStatus.Active {
			return errors.New("worker has no valid price table")
		}

		ut = ws.PriceTableStatus.UpdateTime
		et = ws.PriceTableStatus.ExpiryTime
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// Wait until after the price table is set to update, note we don't gain
	// anything by waiting for this inside the build.Retry as we know when it
	// won't trigger before the update time.
	time.Sleep(time.Until(ut))

	// Verify in a retry that the price table's updateTime and expiryTime have
	// been set to new dates in the future, indicating a successful price table
	// update.
	err = build.Retry(100, 100*time.Millisecond, func() error {
		rwg, err := r.RenterWorkersGet()
		if err != nil {
			return err
		}

		var ws *skymodules.WorkerStatus
		for i := range rwg.Workers {
			worker := rwg.Workers[i]
			if worker.HostPubKey.Equals(host) {
				ws = &worker
				break
			}
		}
		if ws == nil {
			return errors.New("worker not found")
		}

		if !(ws.PriceTableStatus.UpdateTime.After(ut) && ws.PriceTableStatus.ExpiryTime.After(et)) {
			return errors.New("updatedTime and expiryTime have not been updated yet, indicating the price table has not been renewed")
		}

		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// testSingleFileGet is a subtest that uses an existing TestGroup to test if
// using the single file API endpoint works
func testSingleFileGet(t *testing.T, tg *siatest.TestGroup) {
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}
	// Grab the first of the group's renters
	renter := tg.Renters()[0]
	// Upload file, creating a piece for each host in the group
	dataPieces := uint64(2)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	fileSize := 100 + siatest.Fuzz()
	_, _, err := renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}

	// Get all files from Renter
	files, err := renter.Files(false)
	if err != nil {
		t.Fatal("Failed to get renter files: ", err)
	}

	// Loop over files and compare against single file endpoint
	for i := range files {
		// Get Single File
		rf, err := renter.RenterFileGet(files[i].SiaPath)
		if err != nil {
			t.Fatal(err)
		}
		// Compare File result and Files Results, check the fields which are
		// expected to be stable between accesses of the file.
		if files[i].Available != rf.File.Available {
			t.Error("mismatch")
		}
		if files[i].CipherType != rf.File.CipherType {
			t.Error("mismatch")
		}
		if files[i].CreateTime != rf.File.CreateTime {
			t.Error("mismatch")
		}
		if files[i].Filesize != rf.File.Filesize {
			t.Error("mismatch")
		}
		if files[i].LocalPath != rf.File.LocalPath {
			t.Error("mismatch")
		}
		if files[i].FileMode != rf.File.FileMode {
			t.Error("mismatch")
		}
		if files[i].NumStuckChunks != rf.File.NumStuckChunks {
			t.Error("mismatch")
		}
		if files[i].OnDisk != rf.File.OnDisk {
			t.Error("mismatch")
		}
		if files[i].Recoverable != rf.File.Recoverable {
			t.Error("mismatch")
		}
		if files[i].Renewing != rf.File.Renewing {
			t.Error("mismatch")
		}
		if files[i].SiaPath != rf.File.SiaPath {
			t.Error("mismatch")
		}
		if files[i].Stuck != rf.File.Stuck {
			t.Error("mismatch")
		}
	}
}

// testCancelAsyncDownload tests that cancelling an async download aborts the
// download and sets the correct fields.
func testCancelAsyncDownload(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	renter := tg.Renters()[0]
	// Upload file, creating a piece for each host in the group
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	fileSize := 10 * modules.SectorSize
	_, remoteFile, err := renter.UploadNewFileBlocking(int(fileSize), dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	// Set a ratelimit that only allows for downloading a sector every second.
	if err := renter.RenterRateLimitPost(int64(modules.SectorSize), 0); err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := renter.RenterRateLimitPost(0, 0); err != nil {
			t.Fatal(err)
		}
	}()
	// Download the file asynchronously.
	dst := filepath.Join(renter.FilesDir().Path(), "canceled_download.dat")
	cancelID, err := renter.RenterDownloadGet(remoteFile.SiaPath(), dst, 0, fileSize, true, true, false)
	if err != nil {
		t.Fatal(err)
	}
	// Sometimes wait a second to not always cancel the download right
	// away.
	time.Sleep(time.Second * time.Duration(fastrand.Intn(2)))
	// Cancel the download.
	if err := renter.RenterCancelDownloadPost(cancelID); err != nil {
		t.Fatal(err)
	}
	// Get the download info.
	rdg, err := renter.RenterDownloadsGet()
	if err != nil {
		t.Fatal(err)
	}
	var di *api.DownloadInfo
	for i := range rdg.Downloads {
		d := rdg.Downloads[i]
		if remoteFile.SiaPath() == d.SiaPath && dst == d.Destination {
			di = &d
			break
		}
	}
	if di == nil {
		t.Fatal("couldn't find download")
	}
	// Make sure the download was cancelled.
	if !di.Completed {
		t.Fatal("download is not marked as completed")
	}
	if di.Received >= fileSize {
		t.Fatal("the download finished successfully")
	}
	if di.Error != skymodules.ErrDownloadCancelled.Error() {
		t.Fatal("error message doesn't match ErrDownloadCancelled")
	}
}

// testUploadDownload is a subtest that uses an existing TestGroup to test if
// uploading and downloading a file works
func testUploadDownload(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	renter := tg.Renters()[0]
	// Upload file, creating a piece for each host in the group
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	fileSize := fastrand.Intn(2*int(modules.SectorSize)) + siatest.Fuzz() + 2 // between 1 and 2*SectorSize + 3 bytes
	localFile, remoteFile, err := renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	// Download the file synchronously directly into memory
	_, _, err = renter.DownloadByStream(remoteFile)
	if err != nil {
		t.Fatal(err)
	}
	// Download the file synchronously to a file on disk
	_, _, err = renter.DownloadToDisk(remoteFile, false)
	if err != nil {
		t.Fatal(err)
	}
	// Download the file asynchronously and wait for the download to finish.
	_, localFile, err = renter.DownloadToDisk(remoteFile, true)
	if err != nil {
		t.Error(err)
	}
	if err := renter.WaitForDownload(localFile, remoteFile); err != nil {
		t.Error(err)
	}
	// Stream the file.
	_, err = renter.Stream(remoteFile)
	if err != nil {
		t.Fatal(err)
	}
	// Stream the file partially a few times. At least 1 byte is streamed.
	for i := 0; i < 5; i++ {
		from := fastrand.Intn(fileSize - 1)             // [0..fileSize-2]
		to := from + 1 + fastrand.Intn(fileSize-from-1) // [from+1..fileSize-1]
		_, err = renter.StreamPartial(remoteFile, localFile, uint64(from), uint64(to))
		if err != nil {
			t.Fatal(err)
		}
	}
	// Download the file again with root set.
	rootPath, err := remoteFile.SiaPath().Rebase(skymodules.RootSiaPath(), skymodules.UserFolder)
	if err != nil {
		t.Fatal(err)
	}
	dst := filepath.Join(renter.FilesDir().Path(), "root.dat")
	_, err = renter.RenterDownloadGet(rootPath, dst, 0, uint64(fileSize), false, true, true)
	if err != nil {
		t.Fatal(err)
	}
	dst = filepath.Join(renter.FilesDir().Path(), "root2.dat")
	_, err = renter.RenterDownloadFullGet(rootPath, dst, false, true)
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = renter.RenterDownloadHTTPResponseGet(rootPath, 0, uint64(fileSize), true, true)
	if err != nil {
		t.Fatal(err)
	}
	_, err = renter.RenterStreamGet(rootPath, false, true)
	if err != nil {
		t.Fatal(err)
	}
	_, err = renter.RenterStreamPartialGet(rootPath, 0, uint64(fileSize), false, true)
	if err != nil {
		t.Fatal(err)
	}
}

// testUploadWithAndWithoutForceParameter is a subtest that uses an existing TestGroup to test if
// uploading an existing file is successful when setting 'force' to 'true' and 'force' set to 'false'
func testUploadWithAndWithoutForceParameter(t *testing.T, tg *siatest.TestGroup) {
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}
	// Grab the first of the group's renters
	renter := tg.Renters()[0]

	// Upload a file, then try to overwrite the file with the force flag set.
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	fileSize := 100 + siatest.Fuzz()
	localFile, _, err := renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	_, err = renter.UploadBlocking(localFile, dataPieces, parityPieces, true)
	if err != nil {
		t.Fatal("Failed to force overwrite a file when specifying 'force=true': ", err)
	}

	// Upload file, then try to overwrite the file without the force flag set.
	dataPieces = uint64(1)
	parityPieces = uint64(len(tg.Hosts())) - dataPieces
	fileSize = 100 + siatest.Fuzz()
	localFile, _, err = renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	_, err = renter.UploadBlocking(localFile, dataPieces, parityPieces, false)
	if err == nil {
		t.Fatal("File overwritten without specifying 'force=true'")
	}

	// Try to upload a file with the force flag set.
	dataPieces = uint64(1)
	parityPieces = uint64(len(tg.Hosts())) - dataPieces
	fileSize = 100 + siatest.Fuzz()
	localFile, _, err = renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, true)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	_, err = renter.UploadBlocking(localFile, dataPieces, parityPieces, false)
	if err == nil {
		t.Fatal("File overwritten without specifying 'force=true'")
	}

	// Try to upload a file with the force flag set.
	dataPieces = uint64(1)
	parityPieces = uint64(len(tg.Hosts())) - dataPieces
	fileSize = 100 + siatest.Fuzz()
	localFile, _, err = renter.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, true)
	if err != nil {
		t.Fatal("Failed to upload a file for testing: ", err)
	}
	_, err = renter.UploadBlocking(localFile, dataPieces, parityPieces, true)
	if err != nil {
		t.Fatal("Failed to force overwrite a file when specifying 'force=true': ", err)
	}
}

// testZeroByteFile tests uploading and downloading a 0 and 1 byte file
func testZeroByteFile(t *testing.T, tg *siatest.TestGroup) {
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}
	// Grab renter
	r := tg.Renters()[0]

	// Create 0 and 1 byte file
	zeroByteFile := 0
	oneByteFile := 1

	// Define helper function
	checkFileInfo := func(actualRF, expectedRF skymodules.FileInfo) {
		// Check redundancy and upload progress
		if expectedRF.Redundancy != actualRF.Redundancy {
			t.Errorf("Expected Redundancy to be %v, got %v", expectedRF.Redundancy, actualRF.Redundancy)
		}
		if expectedRF.UploadProgress != actualRF.UploadProgress {
			t.Errorf("Expected UploadProgress to be %v, got %v", expectedRF.UploadProgress, actualRF.UploadProgress)
		}
		// Check health information
		if expectedRF.Finished != actualRF.Finished {
			t.Errorf("Expected Finished to be %v, got %v", expectedRF.Finished, actualRF.Finished)
		}
		if expectedRF.Health != actualRF.Health {
			t.Errorf("Expected Health to be %v, got %v", expectedRF.Health, actualRF.Health)
		}
		if expectedRF.MaxHealth != actualRF.MaxHealth {
			t.Errorf("Expected MaxHealth to be %v, got %v", expectedRF.MaxHealth, actualRF.MaxHealth)
		}
		if expectedRF.MaxHealthPercent != actualRF.MaxHealthPercent {
			t.Errorf("Expected MaxHealthPercent to be %v, got %v", expectedRF.MaxHealthPercent, actualRF.MaxHealthPercent)
		}
		if expectedRF.NumStuckChunks != actualRF.NumStuckChunks {
			t.Errorf("Expected NumStuckChunks to be %v, got %v", expectedRF.NumStuckChunks, actualRF.NumStuckChunks)
		}
		if expectedRF.Stuck != actualRF.Stuck {
			t.Errorf("Expected Stuck to be %v, got %v", expectedRF.Stuck, actualRF.Stuck)
		}
		if expectedRF.StuckHealth != actualRF.StuckHealth {
			t.Errorf("Expected StuckHealth to be %v, got %v", expectedRF.StuckHealth, actualRF.StuckHealth)
		}
		// Check Repair information
		if expectedRF.RepairBytes != actualRF.RepairBytes {
			t.Errorf("Expected RepairBytes to be %v, got %v", expectedRF.RepairBytes, actualRF.RepairBytes)
		}
		if expectedRF.StuckBytes != actualRF.StuckBytes {
			t.Errorf("Expected StuckBytes to be %v, got %v", expectedRF.StuckBytes, actualRF.StuckBytes)
		}
	}

	// Test uploading 0 byte file
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces
	redundancy := float64((dataPieces + parityPieces) / dataPieces)
	_, zeroRF, err := r.UploadNewFile(zeroByteFile, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}
	// Get zerobyte file
	rf, err := r.File(zeroRF)
	if err != nil {
		t.Fatal(err)
	}
	expectedRF := skymodules.FileInfo{
		Redundancy:       redundancy,
		UploadProgress:   100,
		Finished:         true,
		Health:           0,
		MaxHealth:        0,
		MaxHealthPercent: 100,
		NumStuckChunks:   0,
		Stuck:            false,
		StuckHealth:      0,
	}
	checkFileInfo(rf, expectedRF)

	// Get the same file using the /renter/files endpoint with 'cached' set to
	// true.
	rfs, err := r.Files(true)
	if err != nil {
		t.Fatal(err)
	}
	var rf2 skymodules.FileInfo
	var found bool
	for _, file := range rfs {
		if file.SiaPath.Equals(rf.SiaPath) {
			found = true
			rf2 = file
			break
		}
	}
	if !found {
		t.Fatal("couldn't find uploaded file using /renter/files endpoint")
	}
	checkFileInfo(rf2, rf)

	// Test uploading 1 byte file
	_, oneRF, err := r.UploadNewFileBlocking(oneByteFile, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Test downloading 0 byte file
	_, _, err = r.DownloadToDisk(zeroRF, false)
	if err != nil {
		t.Fatal(err)
	}

	// Test downloading 1 byte file
	_, _, err = r.DownloadToDisk(oneRF, false)
	if err != nil {
		t.Fatal(err)
	}
}

// testFileAvailableAndRecoverable checks to make sure that the API properly
// reports if a file is available and/or recoverable
func testFileAvailableAndRecoverable(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	// Check that we have 5 hosts for this test so that the redundancy
	// assumptions work for the test
	if len(tg.Hosts()) != 5 {
		t.Fatal("This test requires 5 hosts")
	}

	// Set fileSize and redundancy for upload
	fileSize := int(modules.SectorSize)
	dataPieces := uint64(4)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces

	// Upload file
	localFile, remoteFile, err := r.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
	if err != nil {
		t.Fatal(err)
	}

	// Get the file info and check if it is available and recoverable. File
	// should be available, recoverable, redundancy >1, and the file should be
	// on disk
	fi, err := r.File(remoteFile)
	if err != nil {
		t.Fatal("failed to get file info", err)
	}
	if fi.Redundancy < 1 {
		t.Fatal("redundancy of file is less than 1:", fi.Redundancy)
	}
	if !fi.OnDisk {
		t.Fatal("file is not on disk")
	}
	if !fi.Available {
		t.Fatal("file is not available")
	}
	if !fi.Recoverable {
		t.Fatal("file is not recoverable")
	}

	// Take down two hosts so that the redundancy drops below 1
	for i := 0; i < 2; i++ {
		if err := tg.RemoveNode(tg.Hosts()[0]); err != nil {
			t.Fatal("Failed to shutdown host", err)
		}
	}
	expectedRedundancy := float64(dataPieces+parityPieces-2) / float64(dataPieces)
	if err := r.WaitForDecreasingRedundancy(remoteFile, expectedRedundancy); err != nil {
		t.Fatal("Redundancy isn't decreasing", err)
	}

	// Get file into, file should not be available because the redundancy is  <1
	// but it should be recoverable because the file is on disk
	fi, err = r.File(remoteFile)
	if err != nil {
		t.Fatal("failed to get file info", err)
	}
	if fi.Redundancy >= 1 {
		t.Fatal("redundancy of file should be less than 1:", fi.Redundancy)
	}
	if !fi.OnDisk {
		t.Fatal("file is not on disk")
	}
	if fi.Available {
		t.Fatal("file should not be available")
	}
	if !fi.Recoverable {
		t.Fatal("file should be recoverable")
	}

	// Delete the file locally.
	if err := localFile.Delete(); err != nil {
		t.Fatal("failed to delete local file", err)
	}

	// Get file into, file should now not be available or recoverable
	fi, err = r.File(remoteFile)
	if err != nil {
		t.Fatal("failed to get file info", err)
	}
	if fi.Redundancy >= 1 {
		t.Fatal("redundancy of file should be less than 1:", fi.Redundancy)
	}
	if fi.OnDisk {
		t.Fatal("file is still on disk")
	}
	if fi.Available {
		t.Fatal("file should not be available")
	}
	if fi.Recoverable {
		t.Fatal("file should not be recoverable")
	}
}

// testSetFileStuck tests that manually setting the 'stuck' field of a file
// works as expected.
func testSetFileStuck(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	// Check if there are already uploaded file we can use.
	rfg, err := r.RenterFilesGet(false)
	if err != nil {
		t.Fatal(err)
	}
	if len(rfg.Files) == 0 {
		// Set fileSize and redundancy for upload
		dataPieces := uint64(len(tg.Hosts()) - 1)
		parityPieces := uint64(len(tg.Hosts())) - dataPieces
		fileSize := int(dataPieces * modules.SectorSize)

		// Upload file
		_, _, err := r.UploadNewFileBlocking(fileSize, dataPieces, parityPieces, false)
		if err != nil {
			t.Fatal(err)
		}
	}
	// Get a file.
	rfg, err = r.RenterFilesGet(false)
	if err != nil {
		t.Fatal(err)
	}
	f := rfg.Files[0]
	// Set stuck to the opposite value it had before.
	if err := r.RenterSetFileStuckPost(f.SiaPath, false, !f.Stuck); err != nil {
		t.Fatal(err)
	}
	// Check if it was set correctly.
	fi, err := r.RenterFileGet(f.SiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if fi.File.Stuck == f.Stuck {
		t.Fatalf("Stuck field should be %v but was %v", !f.Stuck, fi.File.Stuck)
	}
	// Set stuck to the original value.
	if err := r.RenterSetFileStuckPost(f.SiaPath, false, f.Stuck); err != nil {
		t.Fatal(err)
	}
	// Check if it was set correctly.
	fi, err = r.RenterFileGet(f.SiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if fi.File.Stuck != f.Stuck {
		t.Fatalf("Stuck field should be %v but was %v", f.Stuck, fi.File.Stuck)
	}
	// Set stuck back once more using the root flag.
	rebased, err := f.SiaPath.Rebase(skymodules.RootSiaPath(), skymodules.UserFolder)
	if err != nil {
		t.Fatal(err)
	}
	if err := r.RenterSetFileStuckPost(rebased, true, !f.Stuck); err != nil {
		t.Fatal(err)
	}
	// Check if it was set correctly.
	fi, err = r.RenterFileGet(f.SiaPath)
	if err != nil {
		t.Fatal(err)
	}
	if fi.File.Stuck == f.Stuck {
		t.Fatalf("Stuck field should be %v but was %v", !f.Stuck, fi.File.Stuck)
	}
}

// testEscapeSiaPath tests that SiaPaths are escaped correctly to handle escape
// characters
func testEscapeSiaPath(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	// Check that we have enough hosts for this test.
	if len(tg.Hosts()) < 2 {
		t.Fatal("This test requires at least 2 hosts")
	}

	// Set fileSize and redundancy for upload
	dataPieces := uint64(1)
	parityPieces := uint64(len(tg.Hosts())) - dataPieces

	// Create Local File
	lf, err := r.FilesDir().NewFile(100)
	if err != nil {
		t.Fatal(err)
	}

	// File names to tests
	names := []string{
		"dollar$sign",
		"and&sign",
		"single`quote",
		"full:colon",
		"semi;colon",
		"hash#tag",
		"percent%sign",
		"at@sign",
		"less<than",
		"greater>than",
		"equal=to",
		"question?mark",
		"open[bracket",
		"close]bracket",
		"open{bracket",
		"close}bracket",
		"carrot^top",
		"pipe|pipe",
		"tilda~tilda",
		"plus+sign",
		"minus-sign",
		"under_score",
		"comma,comma",
		"apostrophy's",
		`quotation"marks`,
	}
	for _, s := range names {
		// Create SiaPath
		siaPath, err := skymodules.NewSiaPath(s)
		if err != nil {
			t.Fatal(err)
		}

		// Upload file
		_, err = r.Upload(lf, siaPath, dataPieces, parityPieces, false)
		if err != nil {
			t.Fatal(err)
		}

		// Confirm we can get file
		_, err = r.RenterFileGet(siaPath)
		if err != nil {
			t.Fatal(err)
		}
	}
}

// testValidateSiaPath tests the validate siapath endpoint
func testValidateSiaPath(t *testing.T, tg *siatest.TestGroup) {
	// Grab the first of the group's renters
	r := tg.Renters()[0]

	// Create siapaths to test
	var pathTests = []struct {
		path  string
		valid bool
	}{
		{`\\some\\windows\\path`, true},
		{"valid/siapath", true},
		{"../../../directory/traversal", false},
		{"testpath", true},
		{"valid/siapath/../with/directory/traversal", false},
		{"validpath/test", true},
		{"..validpath/..test", true},
		{"./invalid/path", false},
		{".../path", true},
		{"valid./path", true},
		{"valid../path", true},
		{"valid/path./test", true},
		{"valid/path../test", true},
		{"test/path", true},
		{"/leading/slash", false}, // this is not valid through the api because a leading slash is added by the api call so this turns into 2 leading slashes
		{"foo/./bar", false},
		{"", false},
		{"blank/end/", true}, // clean will trim trailing slashes so this is a valid input
		{"double//dash", false},
		{"../", false},
		{"./", false},
		{".", false},
	}
	// Test all siapaths
	for _, pathTest := range pathTests {
		err := r.RenterValidateSiaPathPost(pathTest.path)
		// Verify expected Error
		if err != nil && pathTest.valid {
			t.Fatal("validateSiapath failed on valid path: ", pathTest.path)
		}
		if err == nil && !pathTest.valid {
			t.Fatal("validateSiapath succeeded on invalid path: ", pathTest.path)
		}
	}

	// Create SiaPaths that contain escape characters
	var escapeCharTests = []struct {
		path  string
		valid bool
	}{
		{"dollar$sign", true},
		{"and&sign", true},
		{"single`quote", true},
		{"full:colon", true},
		{"semi;colon", true},
		{"hash#tag", true},
		{"percent%sign", true},
		{"at@sign", true},
		{"less<than", true},
		{"greater>than", true},
		{"equal=to", true},
		{"question?mark", true},
		{"open[bracket", true},
		{"close]bracket", true},
		{"open{bracket", true},
		{"close}bracket", true},
		{"carrot^top", true},
		{"pipe|pipe", true},
		{"tilda~tilda", true},
		{"plus+sign", true},
		{"minus-sign", true},
		{"under_score", true},
		{"comma,comma", true},
		{"apostrophy's", true},
		{`quotation"marks`, true},
	}
	// Test all escape charcter siapaths
	for _, escapeCharTest := range escapeCharTests {
		path := url.PathEscape(escapeCharTest.path)
		err := r.RenterValidateSiaPathPost(path)
		// Verify expected Error
		if err != nil && escapeCharTest.valid {
			t.Fatalf("validateSiapath failed on valid path %v, escaped %v ", escapeCharTest.path, path)
		}
		if err == nil && !escapeCharTest.valid {
			t.Fatalf("validateSiapath succeeded on invalid path %v, escaped %v ", escapeCharTest.path, path)
		}
	}
}

// testNextPeriod confirms that the value for NextPeriod in RenterGET is valid
func testNextPeriod(t *testing.T, tg *siatest.TestGroup) {
	// Grab the renter
	r := tg.Renters()[0]

	// Request RenterGET
	rg, err := r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if reflect.DeepEqual(rg.Settings.Allowance, skymodules.Allowance{}) {
		t.Fatal("test only is valid if the allowance is set")
	}

	// Check Next Period
	currentPeriod, err := r.RenterCurrentPeriod()
	if err != nil {
		t.Fatal(err)
	}
	settings, err := r.RenterSettings()
	if err != nil {
		t.Fatal(err)
	}
	period := settings.Allowance.Period
	nextPeriod := rg.NextPeriod
	if nextPeriod == 0 {
		t.Fatal("NextPeriod should not be zero for a renter with an allowance and contracts")
	}
	if nextPeriod != currentPeriod+period {
		t.Fatalf("expected next period to be %v but got %v", currentPeriod+period, nextPeriod)
	}

	// Verify that NextPeriod is Independent from Contract EndHeight

	// Grab the endheight of the current contracts
	contractMap := make(map[types.FileContractID]types.BlockHeight)
	rc, err := r.RenterContractsGet()
	if err != nil {
		t.Fatal(err)
	}
	endHeight := rc.ActiveContracts[0].EndHeight
	for _, c := range rc.ActiveContracts {
		if endHeight != c.EndHeight {
			t.Fatal("Found unexpected endheight", endHeight, c.EndHeight)
		}
		contractMap[c.ID] = c.EndHeight
	}

	// Mine a Block
	m := tg.Miners()[0]
	err = m.MineBlock()
	if err != nil {
		t.Fatal(err)
	}

	// Cancel the Renter's Allowance
	err = r.RenterAllowanceCancelPost()
	if err != nil {
		t.Fatal(err)
	}

	// Mine a Block
	err = m.MineBlock()
	if err != nil {
		t.Fatal(err)
	}

	// Set an Allowance with a different Period
	allowance := siatest.DefaultAllowance
	allowance.Period += 5
	err = r.RenterPostAllowance(allowance)
	if err != nil {
		t.Fatal(err)
	}

	// Mine a Block
	err = m.MineBlock()
	if err != nil {
		t.Fatal(err)
	}

	// Verify active contracts came back
	//
	// NOTE: not using the siatest.CheckExpectedNumberOfContractsRetry
	// helper because the state of the other contracts is unknown due to
	// this being a group test.
	err = build.Retry(100, 100*time.Millisecond, func() error {
		rc, err := r.RenterContractsGet()
		if err != nil {
			return err
		}
		if len(rc.ActiveContracts) != len(contractMap) {
			return fmt.Errorf("expected %v contracts got %v", len(contractMap), len(rc.ActiveContracts))
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// Check Next Period
	rg, err = r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	currentPeriod, err = r.RenterCurrentPeriod()
	if err != nil {
		t.Fatal(err)
	}
	period = allowance.Period
	nextPeriod = rg.NextPeriod
	if nextPeriod != currentPeriod+period {
		t.Fatalf("expected next period to be %v but got %v", currentPeriod+period, nextPeriod)
	}

	// Verify different from contracts
	rc, err = r.RenterContractsGet()
	if err != nil {
		t.Fatal(err)
	}
	for _, c := range rc.ActiveContracts {
		eh, ok := contractMap[c.ID]
		if !ok {
			t.Fatal("contract not found")
		}
		if eh != c.EndHeight {
			t.Fatalf("eh %v not equal to endHeight %v", eh, c.EndHeight)
		}
		if eh == nextPeriod {
			t.Fatalf("expected eh %v to not equal nextPeriod %v", eh, nextPeriod)
		}
	}
}

// testPauseAndResumeRepairAndUploads tests that the Renter's API endpoint to
// pause and resume the repair and uploads works as intended
func testPauseAndResumeRepairAndUploads(t *testing.T, tg *siatest.TestGroup) {
	// Grab Renter
	r := tg.Renters()[0]
	numHost := len(tg.Hosts())
	hostToAdd := 2

	// Confirm that starting out the Renter's uploads are not paused
	rg, err := r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if rg.Settings.UploadsStatus.Paused {
		t.Fatal("Renter's uploads are paused at the beginning of the test")
	}
	if !rg.Settings.UploadsStatus.PauseEndTime.Equal(time.Time{}) {
		t.Fatalf("Pause end time should be null if the uploads are not paused but was %v", rg.Settings.UploadsStatus.PauseEndTime)
	}

	// Pause Repairs And Uploads with a high duration to ensure that the uploads
	// and repairs don't start before we want them to
	err = r.RenterUploadsPausePost(time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the Renter's uploads are now paused
	rg, err = r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if !rg.Settings.UploadsStatus.Paused {
		t.Fatal("Renter's uploads are not paused but should be")
	}
	if rg.Settings.UploadsStatus.PauseEndTime.Equal(time.Time{}) {
		t.Fatal("Pause end time should not be null if the uploads are paused")
	}

	// Try and Upload a file, the upload post should succeed but the upload
	// progress of the file should never increase because the uploads are
	// paused
	_, rf, err := r.UploadNewFile(100, 1, uint64(numHost+hostToAdd-1), false)
	if err != nil {
		t.Fatal(err)
	}
	for i := 0; i < 5; i++ {
		file, err := r.File(rf)
		if err != nil {
			t.Fatal(err)
		}
		if file.UploadProgress != 0 {
			t.Fatal("UploadProgress is increasing, expected it to stay at 0:", file.UploadProgress)
		}
		time.Sleep(500 * time.Millisecond)
	}

	// Resume Repair
	err = r.RenterUploadsResumePost()
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the Renter's uploads are no longer paused
	rg, err = r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if rg.Settings.UploadsStatus.Paused {
		t.Fatal("Renter's uploads are still paused")
	}
	if !rg.Settings.UploadsStatus.PauseEndTime.Equal(time.Time{}) {
		t.Fatalf("Pause end time should be null if the uploads are not paused but was %v", rg.Settings.UploadsStatus.PauseEndTime)
	}

	// Confirm Upload resumes and gets to the expected redundancy. There aren't
	// enough hosts yet to get to the fullRedundancy
	fullRedundancy := float64(numHost + hostToAdd)
	expectedRedundancy := float64(numHost)
	err = build.Retry(100, 250*time.Millisecond, func() error {
		file, err := r.File(rf)
		if err != nil {
			return err
		}
		if file.Redundancy < expectedRedundancy {
			return fmt.Errorf("redundancy should be %v but was %v", expectedRedundancy, file.Redundancy)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// Pause the repairs and uploads again
	err = r.RenterUploadsPausePost(time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the Renter's uploads are now paused
	rg, err = r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if !rg.Settings.UploadsStatus.Paused {
		t.Fatal("Renter's uploads are not paused but should be")
	}
	if rg.Settings.UploadsStatus.PauseEndTime.Equal(time.Time{}) {
		t.Fatal("Pause end time should not be null if the uploads are paused")
	}

	// Update renter's allowance to require making contracts with the new hosts
	allowance := rg.Settings.Allowance
	allowance.Hosts = uint64(numHost + hostToAdd)
	err = r.RenterPostAllowance(allowance)
	if err != nil {
		t.Fatal(err)
	}

	// Add hosts so upload can get to full redundancy
	_, err = tg.AddNodeN(node.HostTemplate, hostToAdd)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm upload still hasn't reach full redundancy because repairs are
	// paused
	for i := 0; i < 5; i++ {
		file, err := r.File(rf)
		if err != nil {
			t.Fatal(err)
		}
		if file.Redundancy == fullRedundancy {
			t.Fatalf("File Redundancy %v has reached full redundancy %v but shouldn't have", file.Redundancy, fullRedundancy)
		}
		time.Sleep(500 * time.Millisecond)
	}

	// Resume Repair and Upload by calling pause again with a very should time
	// duration so the repairs and uploads restart on their own
	err = r.RenterUploadsPausePost(time.Millisecond)
	if err != nil {
		t.Fatal(err)
	}

	// Confirm file gets to full Redundancy
	err = build.Retry(100, 100*time.Millisecond, func() error {
		file, err := r.File(rf)
		if err != nil {
			return err
		}
		if file.Redundancy < fullRedundancy {
			return fmt.Errorf("redundancy should be %v but was %v", fullRedundancy, file.Redundancy)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	// Confirm the Renter's uploads are no longer paused
	rg, err = r.RenterGet()
	if err != nil {
		t.Fatal(err)
	}
	if rg.Settings.UploadsStatus.Paused {
		t.Fatal("Renter's uploads are still paused")
	}
	if !rg.Settings.UploadsStatus.PauseEndTime.Equal(time.Time{}) {
		t.Fatalf("Pause end time should be null if the uploads are not paused but was %v", rg.Settings.UploadsStatus.PauseEndTime)
	}
}

// testDownloadServedFromDisk tests whether downloads will actually be served
// from disk.
func testDownloadServedFromDisk(t *testing.T, tg *siatest.TestGroup) {
	// Make sure a renter is available for testing.
	if len(tg.Renters()) == 0 {
		renterParams := node.Renter(filepath.Join(renterTestDir(t.Name()), "renter"))
		_, err := tg.AddNodes(renterParams)
		if err != nil {
			t.Fatal(err)
		}
	}
	r := tg.Renters()[0]
	// Upload a file. Choose more datapieces than hosts available to prevent the
	// file from reaching 1x redundancy. That way it will only be downloadable
	// from disk.
	_, rf, err := r.UploadNewFile(int(1000), uint64(len(tg.Hosts())+1), 1, false)
	if err != nil {
		t.Fatal(err)
	}
	// Download it in all ways possible. The download will only succeed if
	// served from disk.
	_, _, err = r.DownloadByStreamWithDiskFetch(rf, false)
	if err != nil {
		t.Fatal(err)
	}
	_, _, err = r.DownloadToDiskWithDiskFetch(rf, false, false)
	if err != nil {
		t.Fatal(err)
	}
	_, err = r.StreamWithDiskFetch(rf, false)
	if err != nil {
		t.Fatal(err)
	}
}

// renterTestDir creates a temporary testing directory for a renter test. This
// should only every be called once per test. Otherwise it will delete the
// directory again.
func renterTestDir(testName string) string {
	path := siatest.TestDir("renter", testName)
	if err := os.MkdirAll(path, persist.DefaultDiskPermissionsTest); err != nil {
		panic(err)
	}
	return path
}
